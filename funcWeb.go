package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httputil"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/joho/godotenv"
)

// create and send HTTP request
// exctract response body (no stream mode) or call stream function 'httpReceiveStream()'
func httpRequestPOST(payloadJSON []byte) (bodyString string, err error) {
	// function name to return if any error occur
	var funcName string = "httpRequestPOST"

	// load env configs
	key, err := loadEnv()
	if err != nil {
		return "", err
	}

	// set api key
	var bearer string = "Bearer " + key

	// create and configure HTTP request
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(payloadJSON))
	if err != nil {
		return "", fmt.Errorf("func %q - error creating HTTP request: %w\n", funcName, err)
	}

	// set HTTP User-Agent Header
	ua, err := createUserAgent(req)
	if err != nil {
		return "", fmt.Errorf("func %q - %w\n", funcName, err)
	}

	// add the necessary HTTP headers
	req.Header.Add("Authorization", bearer)
	req.Header.Add("Content-Type", "application/json; charset=UTF-8")
	req.Header.Add("User-Agent", ua)

	// create HTTP client
	client := &http.Client{
		Timeout: timeout,
	}

	// make HTTP request
	startTime := time.Now()
	resp, errResp := client.Do(req)
	endTime := time.Now()
	if errResp != nil {
		e := strings.ReplaceAll(errResp.Error(), "\"", "'")
		return "", fmt.Errorf("func '%s' - error during POST request: %s\n", funcName, e)
	}
	defer resp.Body.Close()

	// encode elapsed time to csv
	elapsed := endTime.Sub(startTime)
	csvFields[requestDateTime] = startTime.Format(time.DateTime)
	csvFields[responseElapsedTime] = fmt.Sprintf("%.9f", float64(elapsed)/nanosec)

	// check HTTP response status
	if resp.StatusCode != http.StatusOK {
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return "", fmt.Errorf("func %q - error reading HTTP response body: %w\n", funcName, err)
		}
		errDetails := fmt.Sprintf("// ------ ERROR DETAILS ------ //\n%s", body)
		return "", fmt.Errorf("func %q - error during HTTP transaction: %s\n\n%s\nmore details: %s", funcName, resp.Status, errDetails, urlErrorCodesDocs)
	}

	// check if stream was requested
	if stream {
		reader := bufio.NewReader(resp.Body)
		if err := httpReceiveStream(reader); err != nil {
			return "", err
		}
		return "", nil
	}

	// parse HTTP response
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("func %q - error reading HTTP response body: %w\n", funcName, err)
	}

	return string(body), nil
}

func httpRequest(httpMethod, url string, requiretAuth bool) (responseBody string, err error) {
	// function name to return if any error occur
	var funcName string = "httpRequest"

	// load env configs
	key, err := loadEnv()
	if err != nil {
		return "", err
	}

	// set api key
	var bearer string = "Bearer " + key

	// create HTTP client
	client := &http.Client{
		Timeout: timeout,
	}

	// configure HTTP client
	req, err := http.NewRequest(httpMethod, url, nil)
	if err != nil {
		return "", fmt.Errorf("func %q - error creating HTTP request: %w\n", funcName, err)
	}

	// set HTTP User-Agent Header
	ua, err := createUserAgent(req)
	if err != nil {
		return "", fmt.Errorf("func %q - %w\n", funcName, err)
	}

	// add headers
	req.Header.Add("User-Agent", ua)

	// check if any auth is required
	if requiretAuth && !flagEndpointStatus {
		req.Header.Add("Authorization", bearer)
	}

	// if needed add more headers
	if flagBatchStatus != "" || flagBatchList {
		req.Header.Add("Content-Type", "application/json; charset=UTF-8")
	}

	// if requested from File API preview payload and return
	if (flagPreview && flagModel == "") && flagWeb == "" {
		requestString := fmt.Sprintf("[Content-Type]\n%s\n\n[BODY]\n%v\n\n[URL]\n%s", req.Header.Get("Content-Type"), req.Body, url)
		return requestString, nil
	}

	// make HTTP request
	resp, err := client.Do(req)
	if err != nil {
		return "", fmt.Errorf("func %q - error during HTTP transaction: %w\n", funcName, err)
	}
	defer resp.Body.Close()

	// extract body from response
	bodyString, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("func %q - error reading HTML body: %w\n", funcName, err)
	}

	return string(bodyString), nil
}

// parse 'STREAM mode' response
func httpReceiveStream(reader *bufio.Reader) error {
	// function name to return if any error occur
	var funcName string = "httpReceiveStream"

	var checkModelUsed bool = true
	var m string          // hold used model name
	var b strings.Builder // build stream response for stream words conuting purpose

	for {
		// Set a read timeout for each iteration
		ctx, cancel := context.WithTimeout(context.Background(), timeoutChunk)
		defer cancel()

		select {
		case <-ctx.Done():
			return fmt.Errorf("func %q - timeout reading response body\n", funcName)
		default:
			// parse io reader
			line, err := reader.ReadBytes('\n')
			if err != nil {
				// check end of stream
				if errors.Is(err, io.EOF) {
					outputWordsCount = len(strings.Fields(b.String())) // count output words
					if flagModelUsed {
						fmt.Printf("\n%s\n", m)
					}
					return nil
				}
				return fmt.Errorf("func %q - error reading HTTP response body: %w\n", funcName, err)
			}

			// print raw response (if '--raw' option was given)
			if flagRaw {
				fmt.Printf("%s", string(line))
				if flagCount != "" && !strings.Contains(flagCount, "words") {
					continue
				}
			}

			// sanitize response
			data := bytes.TrimLeft(line, "data: ")
			data = bytes.TrimRight(data, "\n")

			// check if 'data' contain any data (empty lines)
			if len(data) <= 1 {
				continue
			}

			// check for last chunk
			if strings.Contains(string(data), "DONE") {
				fmt.Println()
				continue
			}

			// print response chunk json formatted
			if flagJson {
				j, err := prettyPrintJsonString(string(data))
				if err != nil {
					return err
				}
				fmt.Printf("%s\n", j)
				if flagCount != "" && !strings.Contains(flagCount, "words") {
					continue
				}
			}

			// --- parse response
			var responseStream responseStream
			err = json.Unmarshal(data, &responseStream)
			if err != nil {
				return fmt.Errorf("func %q - error extracting JSON response: %w\n", funcName, err)
			}
			if checkModelUsed {
				m = responseStream.Model
				checkModelUsed = false
			}
			if len(responseStream.Choices) > 0 {
				if responseStream.Choices[0].Delta != nil {
					if len(responseStream.Choices[0].Delta.Content) > 0 {
						// build response string for words counting purpose
						if _, err := b.WriteString(responseStream.Choices[0].Delta.Content); err != nil {
							return fmt.Errorf("func %q - error building stream response\n", funcName)
						}
						// output char by char, for smooth streaming
						for _, c := range responseStream.Choices[0].Delta.Content {
							fmt.Printf("%c", c)
							time.Sleep(streamCharsPause)
						}
					}
				}
				continue
			}
			// token count
			if responseStream.Usage != nil {
				streamTokens[inputTokens] = responseStream.Usage.PromptTokens
				streamTokens[outputTokens] = responseStream.Usage.CompletionTokens
				streamTokens[totalTokens] = responseStream.Usage.TotalTokens
				continue
			}
		}
		cancel()
	}
}

// load API key
func loadEnv() (key string, err error) {
	// function name to return if any error occur
	var funcName string = "loadEnv"

	// get env
	switch {
	case flagFile != "":
		// from supplied .env file
		err := godotenv.Load(flagFile)
		if err != nil {
			return "", fmt.Errorf("func %q - error loading env file %s", funcName, flagFile)
		}
	default:
		// from os environment
		envVar := os.Getenv(env)

		// from default .env file
		cfgEnv := os.Getenv(cfgRootDir) + cfgDir + cfgFile
		errFile := godotenv.Load(cfgEnv)

		// check result
		if errFile != nil && envVar == "" {
			return "", fmt.Errorf("func %q - error loading default env file or missing env var %q", funcName, env)
		}
	}

	// load env configs
	key, ok := os.LookupEnv(env)
	if !ok || key == "" {
		return "", fmt.Errorf("func %q - openai API KEY not given", funcName)
	}
	return key, nil
}

func httpRequestFileUploadPOST(filename string) (bodyString string, err error) {
	// function name to return if any error occur
	var funcName string = "httpRequestFileUploadPOST"

	// load env configs
	key, err := loadEnv()
	if err != nil {
		return "", err
	}

	// set api key
	var bearer string = "Bearer " + key

	// Read the file content
	fileContent, err := os.ReadFile(filepath.Clean(filename))
	if err != nil {
		fmt.Println("Error reading file:", err)
		return "", fmt.Errorf("func %q - error reading file %s: %w", funcName, filename, err)
	}

	// Create a new HTTP request
	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	if err := writer.WriteField("purpose", fileApiUploadPurpose); err != nil {
		return "", fmt.Errorf("func %q - error writing 'purpose' field: %w", funcName, err)
	}

	// Create form file for the JSONL file
	part, err := writer.CreateFormFile("file", filename)
	if err != nil {
		return "", fmt.Errorf("func %q - error creating HTTP upload form file %s: %w", funcName, filename, err)
	}
	if _, err := part.Write(fileContent); err != nil {
		return "", fmt.Errorf("func %q - error writing HTTP upload form file %s: %w", funcName, filename, err)
	}

	// Close the multipart writer
	defer writer.Close()

	// create and configure HTTP request
	req, err := http.NewRequest("POST", url, body)
	if err != nil {
		return "", fmt.Errorf("func %q - error creating HTTP request: %w\n", funcName, err)
	}

	// set HTTP User-Agent Header
	ua, err := createUserAgent(req)
	if err != nil {
		return "", fmt.Errorf("func %q - %w\n", funcName, err)
	}

	// add the necessary HTTP headers
	req.Header.Add("Authorization", bearer)
	req.Header.Set("Content-Type", writer.FormDataContentType())
	req.Header.Add("User-Agent", ua)

	// if requested preview payload and return
	if flagPreview {
		bodyString = fmt.Sprintf("[Content-Type]\n%s\n\n[BODY]\n%v\n\n[URL]\n%s", req.Header.Get("Content-Type"), req.Body, url)
		return bodyString, nil
	}

	// create HTTP client
	client := &http.Client{
		Timeout: timeout,
	}

	// make HTTP request
	resp, errResp := client.Do(req)
	if errResp != nil {
		e := strings.ReplaceAll(errResp.Error(), "\"", "'")
		return "", fmt.Errorf("func '%s' - error during POST request: %s\n", funcName, e)
	}
	defer resp.Body.Close()

	// check HTTP response status
	if resp.StatusCode != http.StatusOK {
		body, err := io.ReadAll(resp.Body)
		if err != nil {
			return "", fmt.Errorf("func %q - error reading HTTP response body: %w\n", funcName, err)
		}
		errDetails := fmt.Sprintf("// ------ ERROR DETAILS ------ //\n%s", body)
		return "", fmt.Errorf("func %q - error during HTTP transaction: %s\n\n%s\nmore details: %s", funcName, resp.Status, errDetails, urlErrorCodesDocs)
	}

	// parse HTTP response
	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("func %q - error reading HTTP response body: %w\n", funcName, err)
	}

	return string(respBody), nil
}

// createUserAgent return a custom 'User-Agent'
func createUserAgent(req *http.Request) (ua string, err error) {
	// function name to return if any error occur
	var funcName string = "createUserAgent"

	// load default Go User-Agent
	dump, err := httputil.DumpRequestOut(req, true)
	if err != nil {
		return "", fmt.Errorf("func %q - error retrieving Go default User-Agent: %w\n", funcName, err)
	}
	_, d, _ := strings.Cut(string(dump), "User-Agent: ")
	d, _, _ = strings.Cut(d, "\r\n")

	// create custom UA
	ua = fmt.Sprintf("%s (%s)", customUA, d)

	return ua, nil
}
