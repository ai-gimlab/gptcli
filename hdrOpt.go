package main

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/pborman/getopt/v2"
)

type flagRelated struct {
	excluded map[string][]struct{}
	admitted map[string][]struct{}
	dependOn map[string][]struct{}
	name     string
}

var admittedCommon map[string][]struct{} = map[string][]struct{}{
	"p":             nil,
	"preview":       nil,
	"response-raw":  nil,
	"response-json": nil,
	"retries":       nil,
	"retries-wait":  nil,
	"status":        nil,
	"timeout":       nil,
	"f":             nil,
	"file":          nil,
}

// how many flags are used
const flagsCount int = 75

// flag labels
const (
	fakeRune                      rune   = '#'
	flagTestTimeoutShort          rune   = fakeRune
	flagTestTimeoutLong           string = "test-timeout"
	flagHelpShort                 rune   = fakeRune
	flagHelpLong                  string = "help"
	flagVersionShort              rune   = fakeRune
	flagVersionLong               string = "version"
	flagTemperatureShort          rune   = 't'
	flagTemperatureLong           string = "temperature"
	flagTopPShort                 rune   = fakeRune
	flagTopPLong                  string = "top-p"
	flagPresencePenaltyShort      rune   = fakeRune
	flagPresencePenaltyLong       string = "pp"
	flagFrequencyPenaltyShort     rune   = fakeRune
	flagFrequencyPenaltyLong      string = "fp"
	flagSeedShort                 rune   = fakeRune
	flagSeedLong                  string = "seed"
	flagFileShort                 rune   = 'f'
	flagFileLong                  string = "file"
	flagListModelsShort           rune   = 'l'
	flagListModelsLong            string = "list-models"
	flagModelShort                rune   = 'm'
	flagModelLong                 string = "model"
	flagResTokShort               rune   = 'r'
	flagResTokLong                string = "response-tokens"
	flagModerationShort           rune   = fakeRune
	flagModerationLong            string = "moderation"
	flagModerationBoolShort       rune   = fakeRune
	flagModerationBoolLong        string = "moderation-bool"
	flagStreamShort               rune   = fakeRune
	flagStreamLong                string = "stream"
	flagCountShort                rune   = 'c'
	flagCountLong                 string = "count"
	flagResponsesShort            rune   = fakeRune
	flagResponsesLong             string = "responses"
	flagAssistantShort            rune   = 'a'
	flagAssistantLong             string = "assistant"
	flagSystemShort               rune   = 's'
	flagSystemLong                string = "system"
	flagJsonShort                 rune   = fakeRune
	flagJsonLong                  string = "response-json"
	flagStopShort                 rune   = fakeRune
	flagStopLong                  string = "stop"
	flagNameShort                 rune   = fakeRune
	flagNameLong                  string = "name"
	flagUserShort                 rune   = fakeRune
	flagUserLong                  string = "user"
	flagLogitBiasShort            rune   = fakeRune
	flagLogitBiasLong             string = "lb"
	flagRawShort                  rune   = fakeRune
	flagRawLong                   string = "response-raw"
	flagPreviewShort              rune   = 'p'
	flagPreviewLong               string = "preview"
	flagFunctionShort             rune   = fakeRune
	flagFunctionLong              string = "function"
	flagFunctionExamplesShort     rune   = fakeRune
	flagFunctionExamplesLong      string = "function-examples"
	flagFunctionCallShort         rune   = fakeRune
	flagFunctionCallLong          string = "function-call"
	flagCsvShort                  rune   = fakeRune
	flagCsvLong                   string = "csv"
	flagModelUsedShort            rune   = fakeRune
	flagModelUsedLong             string = "model-used"
	flagPresetShort               rune   = fakeRune
	flagPresetLong                string = "preset"
	flagPresetSystemShort         rune   = fakeRune
	flagPresetSystemLong          string = "preset-system"
	flagListPresetsShort          rune   = fakeRune
	flagListPresetsLong           string = "list-presets"
	flagWebShort                  rune   = fakeRune
	flagWebLong                   string = "web"
	flagWebSelectShort            rune   = fakeRune
	flagWebSelectLong             string = "web-select"
	flagWebTestShort              rune   = fakeRune
	flagWebTestLong               string = "web-test"
	flagTimeoutShort              rune   = fakeRune
	flagTimeoutLong               string = "timeout"
	flagTimeoutChunckShort        rune   = fakeRune
	flagTimeoutChunkLong          string = "timeout-chunk"
	flagRetriesshort              rune   = fakeRune
	flagRetriesLong               string = "retries"
	flagRetriesWaitShort          rune   = fakeRune
	flagRetriesWaitLong           string = "retries-wait"
	flagFingerprintShort          rune   = fakeRune
	flagFingerprintLong           string = "fingerprint"
	flagResponseFormatShort       rune   = fakeRune
	flagResponseFormatLong        string = "format"
	flagEndpointStatusShort       rune   = fakeRune
	flagEndpointStatusLong        string = "status"
	flagPreviousPromptShort       rune   = fakeRune
	flagPreviousPromptLong        string = "previous-prompt"
	flagToolShort                 rune   = fakeRune
	flagToolLong                  string = "tool"
	flagToolContentShort          rune   = fakeRune
	flagToolContentLong           string = "tool-content"
	flagVisionShort               rune   = 'v'
	flagVisionLong                string = "vision"
	flagVisionDetailShort         rune   = fakeRune
	flagVisionDetailLong          string = "vision-detail"
	flagLogProbsShort             rune   = fakeRune
	flagLogProbsLong              string = "logprobs"
	flagTopLogProbsShort          rune   = fakeRune
	flagTopLogProbsLong           string = "top-logprobs"
	flagEmbedShort                rune   = 'e'
	flagEmbedLong                 string = "embed"
	flagEmbedEncodingFormatShort  rune   = fakeRune
	flagEmbedEncodingFormatLong   string = "encoding-format"
	flagEmbedDimensionsShort      rune   = fakeRune
	flagEmbedDimensionsLong       string = "embed-dimensions"
	flagDefaultsShort             rune   = fakeRune
	flagDefaultsLong              string = "defaults"
	flagBatchPrepareShort         rune   = fakeRune
	flagBatchPrepareLong          string = "batch-prepare"
	flagBatchInputShort           rune   = fakeRune
	flagBatchInputLong            string = "batch-input"
	flagBatchIdShort              rune   = fakeRune
	flagBatchIdLong               string = "batch-id"
	flagFileApiUploadShort        rune   = fakeRune
	flagFileApiUploadLong         string = "fupload"
	flagParallelToolCallsShort    rune   = fakeRune
	flagParallelToolCallsLong     string = "function-noparallel"
	flagBatchCreateShort          rune   = fakeRune
	flagBatchCreateLong           string = "batch-create"
	flagBatchEndpointShort        rune   = fakeRune
	flagBatchEndpointLong         string = "batch-endpoint"
	flagBatchMetadataShort        rune   = fakeRune
	flagBatchMetadataLong         string = "batch-metadata"
	flagBatchStatusShort          rune   = fakeRune
	flagBatchStatusLong           string = "batch-status"
	flagBatchListShort            rune   = fakeRune
	flagBatchListLong             string = "batch-list"
	flagBatchListLimitShort       rune   = fakeRune
	flagBatchListLimitLong        string = "batch-listlimit"
	flagBatchListAfterShort       rune   = fakeRune
	flagBatchListAfterLong        string = "batch-listafter"
	flagBatchCancelShort          rune   = fakeRune
	flagBatchCancelLong           string = "batch-cancel"
	flagBatchResultShort          rune   = fakeRune
	flagBatchResultLong           string = "batch-result"
	flagFileApiRetrieveShort      rune   = fakeRune
	flagFileApiRetrieveLong       string = "fretrieve"
	flagFileApiSaveShort          rune   = fakeRune
	flagFileApiSaveLong           string = "fsave"
	flagFileApiUploadPurposeShort rune   = fakeRune
	flagFileApiUploadPurposeLong  string = "fpurpose"
	flagFileApiListShort          rune   = fakeRune
	flagFileApiListLong           string = "flist"
	flagFileApiInfoShort          rune   = fakeRune
	flagFileApiInfoLong           string = "finfo"
	flagFileApiDeleteShort        rune   = fakeRune
	flagFileApiDeleteLong         string = "fdelete"
)

// Declare flags and have getopt return pointers to the values.
var (
	flagTestTimeout          bool
	flagHelp                 bool
	flagVersion              bool
	flagFile                 string
	flagListModels           bool
	flagModel                string
	flagResTok               int
	flagTemperature          float64
	flagTopP                 float64
	flagModeration           bool
	flagModerationBool       bool
	flagStream               bool
	flagCount                string
	flagResponses            int
	flagAssistant            string
	flagSystem               string
	flagPresencePenalty      float64
	flagFrequencyPenalty     float64
	flagSeed                 int
	flagJson                 bool
	flagStop                 string
	flagName                 string
	flagUser                 string
	flagLogitBias            string
	flagRaw                  bool
	flagPreview              bool
	flagFunction             string
	flagFunctionExamples     bool
	flagFunctionCall         string
	flagCsv                  string
	flagModelUsed            bool
	flagPreset               string
	flagPresetSystem         bool
	flagListPresets          bool
	flagWeb                  string
	flagWebSelect            string
	flagWebTest              bool
	flagTimeout              int64
	flagTimeoutChunk         int64
	flagRetries              int
	flagRetriesWait          int
	flagFingerprint          bool
	flagResponseFormat       string
	flagEndpointStatus       bool
	flagPreviousPrompt       string
	flagTool                 string
	flagToolContent          string
	flagVision               string
	flagVisionDetail         string
	flagLogProbs             bool
	flagTopLogProbs          int
	flagEmbed                bool
	flagEmbedEncodingFormat  bool
	flagEmbedDimensions      int
	flagDefaults             bool
	flagBatchPrepare         bool
	flagBatchInput           string
	flagBatchId              string
	flagFileApiUpload        string
	flagParallelToolCalls    bool
	flagBatchCreate          string
	flagBatchEndpoint        string
	flagBatchMetadata        string
	flagBatchStatus          string
	flagBatchList            bool
	flagBatchListLimit       int
	flagBatchListAfter       string
	flagBatchCancel          string
	flagBatchResult          string
	flagFileApiRetrieve      string
	flagFileApiSave          string
	flagFileApiUploadPurpose string
	flagFileApiList          bool
	flagFileApiInfo          string
	flagFileApiDelete        string
	flags                    []getopt.Option = make([]getopt.Option, 0, flagsCount)
	flagsCounter             map[string]int  = make(map[string]int)
	flagsTypes               []string        = make([]string, 0, flagsCount)
)

// init vars at program start with 'init()' internal function
func init() {
	flags = append(flags, getopt.FlagLong(&flagTestTimeout, "test-timeout", '\U0001f405', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTimeout))
	flags = append(flags, getopt.FlagLong(&flagHelp, "help", '\U0001f595', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagHelp))
	flags = append(flags, getopt.FlagLong(&flagVersion, "version", '\U0001f6bd', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagVersion))
	flags = append(flags, getopt.FlagLong(&flagFile, flagFileLong, flagFileShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFile))
	flags = append(flags, getopt.FlagLong(&flagListModels, flagListModelsLong, flagListModelsShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagListModels))
	flags = append(flags, getopt.FlagLong(&flagModel, flagModelLong, flagModelShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagModel))
	flags = append(flags, getopt.FlagLong(&flagResTok, flagResTokLong, flagResTokShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagResTok))
	flags = append(flags, getopt.FlagLong(&flagTemperature, flagTemperatureLong, flagTemperatureShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTemperature))
	flags = append(flags, getopt.FlagLong(&flagTopP, flagTopPLong, '\U0001f601', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTopP))
	flags = append(flags, getopt.FlagLong(&flagModeration, flagModerationLong, '\U0001f596', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagModeration))
	flags = append(flags, getopt.FlagLong(&flagModerationBool, flagModerationBoolLong, '\U0001f503', "").SetGroup("outputFMT"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagModerationBool))
	flags = append(flags, getopt.FlagLong(&flagStream, flagStreamLong, '\U0001f598', "").SetGroup("notStream"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagStream))
	flags = append(flags, getopt.FlagLong(&flagCount, flagCountLong, flagCountShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCount))
	flags = append(flags, getopt.FlagLong(&flagResponses, flagResponsesLong, '\U0001f500', "").SetGroup("notStream"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagResponses))
	flags = append(flags, getopt.FlagLong(&flagAssistant, flagAssistantLong, flagAssistantShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagAssistant))
	flags = append(flags, getopt.FlagLong(&flagSystem, flagSystemLong, flagSystemShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagSystem))
	flags = append(flags, getopt.FlagLong(&flagPresencePenalty, flagPresencePenaltyLong, '\U0001f578', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPresencePenalty))
	flags = append(flags, getopt.FlagLong(&flagFrequencyPenalty, flagFrequencyPenaltyLong, '\U0001f501', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFrequencyPenalty))
	flags = append(flags, getopt.FlagLong(&flagSeed, flagSeedLong, '\U0001f001', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagSeed))
	flags = append(flags, getopt.FlagLong(&flagJson, flagJsonLong, '\U0001f002', "").SetGroup("outputFMT"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagJson))
	flags = append(flags, getopt.FlagLong(&flagStop, flagStopLong, '\U0001f504', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagStop))
	flags = append(flags, getopt.FlagLong(&flagName, flagNameLong, '\U0001f505', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagName))
	flags = append(flags, getopt.FlagLong(&flagUser, flagUserLong, '\U0001f506', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagUser))
	flags = append(flags, getopt.FlagLong(&flagLogitBias, flagLogitBiasLong, '\U0001f507', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagLogitBias))
	flags = append(flags, getopt.FlagLong(&flagRaw, flagRawLong, '\U0001f508', "").SetGroup("outputFMT"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagRaw))
	flags = append(flags, getopt.FlagLong(&flagPreview, flagPreviewLong, flagPreviewShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPreview))
	flags = append(flags, getopt.FlagLong(&flagFunction, flagFunctionLong, '\U0001f511', "").SetGroup("notStream"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFunction))
	flags = append(flags, getopt.FlagLong(&flagFunctionExamples, flagFunctionExamplesLong, '\U0001f510', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFunctionExamples))
	flags = append(flags, getopt.FlagLong(&flagFunctionCall, flagFunctionCallLong, '\U0001f512', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFunctionCall))
	flags = append(flags, getopt.FlagLong(&flagCsv, flagCsvLong, '\U0001f513', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagCsv))
	flags = append(flags, getopt.FlagLong(&flagModelUsed, flagModelUsedLong, '\U0001f514', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagModelUsed))
	flags = append(flags, getopt.FlagLong(&flagPreset, flagPresetLong, '\U0001f515', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPreset))
	flags = append(flags, getopt.FlagLong(&flagListPresets, flagListPresetsLong, '\U0001f516', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagListPresets))
	flags = append(flags, getopt.FlagLong(&flagWeb, flagWebLong, '\U0001f517', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagWeb))
	flags = append(flags, getopt.FlagLong(&flagWebSelect, flagWebSelectLong, '\U0001f518', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagWebSelect))
	flags = append(flags, getopt.FlagLong(&flagWebTest, flagWebTestLong, '\U0001f519', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagWebTest))
	flags = append(flags, getopt.FlagLong(&flagPresetSystem, flagPresetSystemLong, '\U0001f520', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPresetSystem))
	flags = append(flags, getopt.FlagLong(&flagTimeout, flagTimeoutLong, '\U0001f521', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTimeout))
	flags = append(flags, getopt.FlagLong(&flagTimeoutChunk, flagTimeoutChunkLong, '\U0001f522', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTimeoutChunk))
	flags = append(flags, getopt.FlagLong(&flagRetries, flagRetriesLong, '\U0001f523', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagRetries))
	flags = append(flags, getopt.FlagLong(&flagRetriesWait, flagRetriesWaitLong, '\U0001f023', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagRetriesWait))
	flags = append(flags, getopt.FlagLong(&flagFingerprint, flagFingerprintLong, '\U0001f524', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFingerprint))
	flags = append(flags, getopt.FlagLong(&flagResponseFormat, flagResponseFormatLong, '\U0001f525', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagResponseFormat))
	flags = append(flags, getopt.FlagLong(&flagEndpointStatus, flagEndpointStatusLong, '\U0001f526', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagEndpointStatus))
	flags = append(flags, getopt.FlagLong(&flagPreviousPrompt, flagPreviousPromptLong, '\U0001f527', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagPreviousPrompt))
	flags = append(flags, getopt.FlagLong(&flagTool, flagToolLong, '\U0001f528', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTool))
	flags = append(flags, getopt.FlagLong(&flagToolContent, flagToolContentLong, '\U0001f529', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagToolContent))
	flags = append(flags, getopt.FlagLong(&flagVision, flagVisionLong, flagVisionShort, ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagVision))
	flags = append(flags, getopt.FlagLong(&flagVisionDetail, flagVisionDetailLong, '\U0001f530', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagVisionDetail))
	flags = append(flags, getopt.FlagLong(&flagLogProbs, flagLogProbsLong, '\U0001f531', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagLogProbs))
	flags = append(flags, getopt.FlagLong(&flagTopLogProbs, flagTopLogProbsLong, '\U0001f532', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagTopLogProbs))
	flags = append(flags, getopt.FlagLong(&flagEmbed, flagEmbedLong, flagEmbedShort, "").SetGroup("notStream"))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagEmbed))
	flags = append(flags, getopt.FlagLong(&flagEmbedEncodingFormat, flagEmbedEncodingFormatLong, '\U0001f533', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagEmbedEncodingFormat))
	flags = append(flags, getopt.FlagLong(&flagEmbedDimensions, flagEmbedDimensionsLong, '\U0001f534', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagEmbed))
	flags = append(flags, getopt.FlagLong(&flagDefaults, flagDefaultsLong, '\U0001f535', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagDefaults))
	flags = append(flags, getopt.FlagLong(&flagBatchPrepare, flagBatchPrepareLong, '\U0001f536', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagBatchPrepare))
	flags = append(flags, getopt.FlagLong(&flagBatchInput, flagBatchInputLong, '\U0001f537', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagBatchInput))
	flags = append(flags, getopt.FlagLong(&flagBatchId, flagBatchIdLong, '\U0001f538', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagBatchId))
	flags = append(flags, getopt.FlagLong(&flagFileApiUpload, flagFileApiUploadLong, '\U0001f539', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFileApiUpload))
	flags = append(flags, getopt.FlagLong(&flagParallelToolCalls, flagParallelToolCallsLong, '\U0001f540', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagParallelToolCalls))
	flags = append(flags, getopt.FlagLong(&flagBatchCreate, flagBatchCreateLong, '\U0001f541', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagBatchCreate))
	flags = append(flags, getopt.FlagLong(&flagBatchEndpoint, flagBatchEndpointLong, '\U0001f542', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagBatchEndpoint))
	flags = append(flags, getopt.FlagLong(&flagBatchMetadata, flagBatchMetadataLong, '\U0001f543', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagBatchMetadata))
	flags = append(flags, getopt.FlagLong(&flagBatchStatus, flagBatchStatusLong, '\U0001f544', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagBatchStatus))
	flags = append(flags, getopt.FlagLong(&flagBatchList, flagBatchListLong, '\U0001f545', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagBatchList))
	flags = append(flags, getopt.FlagLong(&flagBatchListLimit, flagBatchListLimitLong, '\U0001f546', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagBatchListLimit))
	flags = append(flags, getopt.FlagLong(&flagBatchListAfter, flagBatchListAfterLong, '\U0001f547', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagBatchListAfter))
	flags = append(flags, getopt.FlagLong(&flagBatchCancel, flagBatchCancelLong, '\U0001f548', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagBatchCancel))
	flags = append(flags, getopt.FlagLong(&flagBatchResult, flagBatchResultLong, '\U0001f549', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagBatchResult))
	flags = append(flags, getopt.FlagLong(&flagFileApiRetrieve, flagFileApiRetrieveLong, '\U0001f550', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFileApiRetrieve))
	flags = append(flags, getopt.FlagLong(&flagFileApiSave, flagFileApiSaveLong, '\U0001f551', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFileApiSave))
	flags = append(flags, getopt.FlagLong(&flagFileApiUploadPurpose, flagFileApiUploadPurposeLong, '\U0001f552', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFileApiUploadPurpose))
	flags = append(flags, getopt.FlagLong(&flagFileApiList, flagFileApiListLong, '\U0001f553', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFileApiList))
	flags = append(flags, getopt.FlagLong(&flagFileApiInfo, flagFileApiInfoLong, '\U0001f554', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFileApiInfo))
	flags = append(flags, getopt.FlagLong(&flagFileApiDelete, flagFileApiDeleteLong, '\U0001f555', ""))
	flagsTypes = append(flagsTypes, fmt.Sprintf("%T", flagFileApiDelete))
}

// due 'getopt' library limitations need the following 'mutually exclusive options' checks
func mutuallyExclusiveOptions() error {
	if flagFileApiUpload != "" {
		admitted := []string{flagFileApiUploadPurposeLong}
		if err := relationsHandler(flagFileApiUploadLong, flagFileApiUploadLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagFileApiDelete != "" {
		admitted := make([]string, 0) // no other options are allowed, apart from the option itself
		if err := relationsHandler(flagFileApiDeleteLong, flagFileApiDeleteLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagFileApiInfo != "" {
		admitted := make([]string, 0) // no other options are allowed, apart from the option itself
		if err := relationsHandler(flagFileApiInfoLong, flagFileApiInfoLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagFileApiList {
		admitted := []string{flagFileApiUploadPurposeLong}
		if err := relationsHandler(flagFileApiListLong, flagFileApiListLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagFileApiRetrieve != "" {
		admitted := []string{flagFileApiSaveLong, flagBatchResultLong}
		if err := relationsHandler(flagFileApiRetrieveLong, flagFileApiRetrieveLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagBatchPrepare {
		excluded := []string{string(flagCountShort), flagCountLong, flagStreamLong, flagWebTestLong, flagCsvLong, flagResponsesLong}
		if err := relationsHandler(flagBatchPrepareLong, flagBatchPrepareLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagBatchResult != "" {
		admitted := []string{flagFileApiRetrieveLong}
		if err := relationsHandler(flagBatchResultLong, flagBatchResultLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagBatchCreate != "" {
		admitted := []string{flagBatchEndpointLong, flagBatchMetadataLong}
		if err := relationsHandler(flagBatchCreateLong, flagBatchCreateLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagBatchList {
		admitted := []string{flagBatchListLimitLong, flagBatchListAfterLong}
		if err := relationsHandler(flagBatchListLong, flagBatchListLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagBatchStatus != "" {
		admitted := make([]string, 0) // no other options are allowed, apart from the option itself
		if err := relationsHandler(flagBatchStatusLong, flagBatchStatusLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagBatchCancel != "" {
		admitted := make([]string, 0) // no other options are allowed, apart from the option itself
		if err := relationsHandler(flagBatchCancelLong, flagBatchCancelLong, admitted, nil, true); err != nil {
			return err
		}
	}
	if flagModeration {
		admitted := make([]string, 0) // no other options are allowed, apart from the option itself
		if err := relationsHandler(flagModerationLong, flagModerationLong, admitted, nil, false); err != nil {
			return err
		}
	}
	if flagModerationBool {
		admitted := make([]string, 0) // no other options are allowed, apart from the option itself
		if err := relationsHandler(flagModerationBoolLong, flagModerationBoolLong, admitted, nil, false); err != nil {
			return err
		}
	}
	if flagEmbed {
		admitted := []string{string(flagEmbedShort), string(flagModelShort), flagModelLong, string(flagCountShort), flagCountLong, flagCsvLong, flagModelUsedLong, flagEmbedDimensionsLong, flagEmbedEncodingFormatLong, flagBatchPrepareLong, flagBatchInputLong, flagBatchIdLong}
		if err := relationsHandler(flagEmbedLong, flagEmbedLong, admitted, nil, false); err != nil {
			return err
		}
	}
	if flagCsv != "" {
		excluded := []string{flagStreamLong, flagWebTestLong, flagResponsesLong, flagModerationLong, flagModerationBoolLong, flagBatchPrepareLong, flagBatchCreateLong, flagBatchListLong, flagBatchStatusLong, flagBatchCancelLong, flagBatchResultLong, flagFileApiUploadLong, flagFileApiDeleteLong, flagFileApiInfoLong, flagFileApiListLong, flagFileApiRetrieveLong}
		if err := relationsHandler(flagCsvLong, flagCsvLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if getopt.GetCount(flagResponsesLong) > 0 {
		excluded := []string{flagCsvLong, flagLogProbsLong, flagStreamLong, flagModerationLong, flagModerationBoolLong}
		if err := relationsHandler(flagResponsesLong, flagResponsesLong, nil, excluded, false); err != nil {
			return err
		}
	}
	if flagFunction != "" {
		excluded := []string{flagResponseFormatLong, flagStreamLong, flagPresetLong}
		if err := relationsHandler(flagFunctionLong, flagFunctionLong, nil, excluded, false); err != nil {
			return err
		}
	}
	/*
			if flagWeb != "" {
				excluded := []string{string(flagSystemShort), flagSystemLong}
				if err := relationsHandler(flagWebLong, flagWebLong, nil, excluded, false); err != nil {
					return err
				}
			}
		if getopt.GetCount(flagFunctionLong) > 0 && getopt.GetCount(flagResponseFormatLong) > 0 {
			return fmt.Errorf("options --%s and --%s are mutually exclusive", flagFunctionLong, flagResponseFormatLong)
		}
	*/
	if flagPreview && flagWebTest {
		return fmt.Errorf("options --%s and --%s are mutually exclusive", flagPreviewLong, flagWebTestLong)
	}
	if getopt.GetCount(flagAssistantLong) > 0 && getopt.GetCount(flagToolLong) > 0 {
		return fmt.Errorf("options --%s and --%s are mutually exclusive", flagAssistantLong, flagToolLong)
	}
	if getopt.GetCount(flagToolLong) > 0 && getopt.GetCount(flagResponseFormatLong) > 0 {
		return fmt.Errorf("options --%s and --%s are mutually exclusive", flagToolLong, flagResponseFormatLong)
	}
	if getopt.GetCount(flagVisionLong) > 0 && (flagTool != "" || flagWeb != "" || flagLogProbs) {
		return fmt.Errorf("options --%s and --%s, --%s or --%s, are mutually exclusive", flagVisionLong, flagToolLong, flagWebLong, flagLogProbsLong)
	}
	return nil
}

// check for flag dependencies (one flag depends from another)
func checkFlagDependencies() error {
	if flagFunctionCall != "" && flagFunction == "" {
		return fmt.Errorf("\aoption '--%s' without '--%s' option", flagFunctionCallLong, flagFunctionLong)
	}
	/*
		if flagFunctionCall != "" && flagFunction == "" {
			return fmt.Errorf("\aoption '--%s' without '--%s' option OR '--%[2]s' option with wrong arguments", flagFunctionCallLong, flagFunctionLong)
		}
	*/
	if (flagWebSelect != "" || flagWebTest) && flagWeb == "" {
		return fmt.Errorf("\aoption '--%s' or '--%s' without '--%s' option", flagWebSelectLong, flagWebTestLong, flagWebLong)
	}
	if flagPresetSystem && flagPreset == "" {
		return fmt.Errorf("\aoption '--%s' without '--%s' option", flagPresetSystemLong, flagPresetLong)
	}
	if getopt.GetCount(flagTimeoutChunkLong) > 0 && !flagStream {
		return fmt.Errorf("\aoption '--%s' without '--%s' option", flagTimeoutChunkLong, flagStreamLong)
	}
	if getopt.GetCount(flagPreviousPromptLong) > 0 && (flagAssistant == "" && flagTool == "") {
		return fmt.Errorf("\aoption '--%s' without '--%s' or '%s' option", flagPreviousPromptLong, flagAssistantLong, flagToolLong)
	}
	if (getopt.GetCount(flagAssistantLong) > 0 || getopt.GetCount(flagToolLong) > 0) && flagPreviousPrompt == "" {
		return fmt.Errorf("\aoption '%s' or '--%s' without  '--%s' option", flagAssistantLong, flagToolLong, flagPreviousPromptLong)
	}
	if getopt.GetCount(flagToolLong) > 0 && flagToolContent == "" {
		return fmt.Errorf("\aoption '--%s' without '--%s' option", flagToolLong, flagToolContentLong)
	}
	if getopt.GetCount(flagVisionDetailLong) > 0 && flagVisionDetail == "" {
		return fmt.Errorf("\aoption '--%s' without '--%s' option", flagVisionDetailLong, flagVisionLong)
	}
	if getopt.GetCount(flagTopLogProbsLong) > 0 && !flagLogProbs {
		return fmt.Errorf("\aoption '--%s' without '--%s' option", flagTopLogProbsLong, flagLogProbsLong)
	}
	if getopt.GetCount(flagEmbedEncodingFormatLong) > 0 && (getopt.GetCount(flagEmbedLong) == 0 || getopt.GetCount(flagEmbedShort) == 0) {
		return fmt.Errorf("\aoption '--%s' without '--%s' option", flagEmbedEncodingFormatLong, flagEmbedLong)
	}
	if getopt.GetCount(flagEmbedDimensionsLong) > 0 && (getopt.GetCount(flagEmbedLong) == 0 || getopt.GetCount(flagEmbedShort) == 0) {
		return fmt.Errorf("\aoption '--%s' without '--%s' option", flagEmbedDimensionsLong, flagEmbedLong)
	}
	if getopt.GetCount(flagRetriesWaitLong) > 0 && getopt.GetCount(flagRetriesLong) == 0 {
		return fmt.Errorf("\aoption '--%s' without  '--%s' option", flagRetriesWaitLong, flagRetriesLong)
	}
	if getopt.GetCount(flagBatchInputLong) > 0 && !flagBatchPrepare {
		return fmt.Errorf("\aoption '--%s' without  '--%s' option", flagBatchInputLong, flagBatchPrepareLong)
	}
	if getopt.GetCount(flagBatchIdLong) > 0 && !flagBatchPrepare {
		return fmt.Errorf("\aoption '--%s' without  '--%s' option", flagBatchIdLong, flagBatchPrepareLong)
	}
	if flagParallelToolCalls && getopt.GetCount(flagFunctionLong) == 0 {
		return fmt.Errorf("\aoption '--%s' without  '--%s' option", flagParallelToolCallsLong, flagFunctionLong)
	}
	if getopt.GetCount(flagBatchEndpointLong) > 0 && getopt.GetCount(flagBatchCreateLong) == 0 {
		return fmt.Errorf("\aoption '--%s' without  '--%s' option", flagBatchEndpointLong, flagBatchCreateLong)
	}
	if getopt.GetCount(flagBatchMetadataLong) > 0 && getopt.GetCount(flagBatchCreateLong) == 0 {
		return fmt.Errorf("\aoption '--%s' without  '--%s' option", flagBatchMetadataLong, flagBatchCreateLong)
	}
	if (getopt.GetCount(flagBatchListLimitLong) > 0 || getopt.GetCount(flagBatchListAfterLong) > 0) && getopt.GetCount(flagBatchListLong) == 0 {
		return fmt.Errorf("\aoption '--%s' or '--%s' without  '--%s' option", flagBatchListLimitLong, flagBatchListAfterLong, flagBatchListLong)
	}
	if (flagBatchResult != "" || flagFileApiSave != "") && getopt.GetCount(flagFileApiRetrieveLong) == 0 {
		return fmt.Errorf("\aoption '--%s' or '--%s' without  '--%s' option", flagBatchResultLong, flagFileApiSaveLong, flagFileApiRetrieveLong)
	}
	if flagFileApiUploadPurpose != "" && (getopt.GetCount(flagFileApiUploadLong) == 0 && getopt.GetCount(flagFileApiListLong) == 0) {
		return fmt.Errorf("\aoption '--%s' without  '--%s' option", flagFileApiUploadPurposeLong, flagFileApiUploadLong)
	}
	return nil
}

// check if any string was provided to string options
func argProvided() error {
	for i := range flags {
		if flags[i].Seen() && !flags[i].IsFlag() && flags[i].Value().String() == "" {
			return fmt.Errorf("option '--%s' missing or wrong argument", flags[i].LongName())
		}
	}
	return nil
}

// check if long option are given with single or double dash
// without this check long option '--abcd', written with single dash, '-abcd', is parsed as a short option '-a'
// try with 'ls' command: 'ls -l -a' and 'ls -l --author' vs 'ls -l -author' (the latter behave as 'ls -l -a')
func checkLongOptionDashes(args []string) error {
	const dash byte = '-'
	const doubleDash string = "--"
	var wasBool bool = true

	// check if arg begin with dash (if begin with dash could be an option)
loopAllGivenArgs:
	for i := 0; i < len(args); i++ {
		var counter int = 0

		// check for empty strings, such as failed shell expansion - eg: "$(cat non-existant-file)"
		if args[i] == "" {
			continue
		}

		// if previous option was not bool, then next arg is (maybe) only the option argument
		if !wasBool {
			wasBool = true
			continue
		}

		// don't check arguments without dash prefix
		if len(args[i]) > 0 && args[i][0] != dash {
			continue
		}

		// don't check arguments with dash prefix + single char - eg: -a, -b, -c, etc...
		if len(args[i]) == 2 && args[i][0] == dash {
			continue
		}

		// remove single or double dash prefix
		s := strings.TrimPrefix(args[i], string(dash))
		s = strings.TrimPrefix(s, string(dash))

		// remove first '=' char, if any (eg. --my-opt=someData)
		s, _, _ = strings.Cut(s, "=")

		// check if string is a number
		if isNum(s) {
			continue
		}

		for _, v := range flags {
			if v.LongName() == s && (args[i][0] == dash && args[i][1] != dash) {
				// if 'longoption' exist, but was given without double dash (eg: --longoption OK, -longoption WRONG)
				return fmt.Errorf("long option with single dash: '-%s' must be '--%[1]s'", s)
			} else if v.LongName() == s {
				// if option is not boolean, then next arg is not an option
				if !v.IsFlag() {
					wasBool = false
				}
				// 'longoption' exist and was given correctly (eg: --longoption)
				continue loopAllGivenArgs
			} else {
				// 'longoption' not found in this inner loop cycle
				counter++
			}
		}

		// option not found
		if counter >= len(flags) {
			return fmt.Errorf("wrong option: '%s'", args[i])
		}
	}
	return nil
}

// used by presets - check if any parameter, such as temperature or model, was given.
// If yes it use command-line parameter setting, instead of preset parameter setting.
func countFlags(args []string, flag string) bool {
	for _, v := range args {
		flagsCounter[v]++
	}
	if _, ok := flagsCounter[flag]; ok {
		return true
	}
	return false
}

// check 'string type' option arguments for "-" char as first char of argument
func checkFlagTypeString() error {
	for i := range flags {
		if flags[i].Seen() && (flagsTypes[i] == "string" && strings.HasPrefix(flags[i].Value().String(), "-")) {
			return fmt.Errorf("\awrong parameter for %s: '%s'", flags[i].Name(), flags[i].Value().String())
		}
	}
	return nil
}

// check if string is a number, integer or float - false = NOT number, true = IS number
func isNum(s string) bool {
	if _, err := strconv.ParseInt(s, 10, 64); err == nil {
		return true
	}
	if _, err := strconv.ParseFloat(s, 64); err == nil {
		return true
	}
	if _, err := strconv.ParseComplex(s, 128); err == nil {
		return true
	}
	return false
}

// callback function for 'getopt.Visit()': ADMITTED
// ref: https://pkg.go.dev/github.com/pborman/getopt/v2#Visit
func checkFlagRelationsAdmitted(option getopt.Option, fr *flagRelated) {
	n := option.Name()
	s := strings.TrimPrefix(n, "-")
	s = strings.TrimPrefix(s, "-")
	if fr.admitted != nil {
		if _, ok := fr.admitted[s]; !ok {
			flagRelationsErr = fmt.Errorf("options '--%s' and '%s' are mutually exclusive", fr.name, n)
		}
	}
}

// callback function for 'getopt.Visit()': EXCLUDED
// ref: https://pkg.go.dev/github.com/pborman/getopt/v2#Visit
func checkFlagRelationsExcluded(option getopt.Option, fr *flagRelated) {
	n := option.Name()
	s := strings.TrimPrefix(n, "-")
	s = strings.TrimPrefix(s, "-")
	if fr.excluded != nil {
		if _, ok := fr.excluded[s]; ok {
			flagRelationsErr = fmt.Errorf("options '--%s' and '%s' are mutually exclusive", fr.name, n)
		}
	}
}

// wrapper for the 'getopt.Visit()' callback function
// ref: https://pkg.go.dev/github.com/pborman/getopt/v2#Visit
func wrapCheckFlagRelations(callback func(getopt.Option, *flagRelated), fr *flagRelated) func(getopt.Option) {
	return func(option getopt.Option) {
		callback(option, fr)
	}
}

// check admitted flags for a given flag name using 'getopt.Visit()'
// ref: https://pkg.go.dev/github.com/pborman/getopt/v2#Visit
func relationsHandler(name, flagLongName string, admitted, excluded []string, checkNonOptArgs bool) error {
	// non-option arguments
	if checkNonOptArgs {
		if len(getopt.Args()) > 0 {
			s := getopt.Args()
			return fmt.Errorf("option '--%s' and arg %q are mutually exclusive", flagLongName, s[0])
		}
	}
	// init flag data
	f := &flagRelated{
		admitted: map[string][]struct{}{
			name: nil,
		},
		excluded: map[string][]struct{}{},
		name:     name,
	}
	// admitted
	if admitted != nil {
		for _, v := range admitted {
			f.admitted[v] = nil
		}
		for k := range admittedCommon {
			f.admitted[k] = nil
		}
		// check relations
		relations := wrapCheckFlagRelations(checkFlagRelationsAdmitted, f)
		getopt.Visit(relations)
	}
	// excluded
	if excluded != nil && len(excluded) > 0 {
		for _, v := range excluded {
			f.excluded[v] = nil
		}
		// check relations
		relations := wrapCheckFlagRelations(checkFlagRelationsExcluded, f)
		getopt.Visit(relations)
	}
	// result
	if flagRelationsErr != nil {
		return flagRelationsErr
	}
	return nil
}
