# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

### Fixed

- **G304 (CWE-22)**: *Potential file inclusion via variable*
- 'function' and 'presets' now are mutually exclusive

### Changed

- Documentation: web content selection example
- Max numbers of requested `top_logprobs`: from 5 to 20
- 'function call' model default to **gpt-4o-mini**

### Added

- Prints the probabilities percentage of `logprobs/top_logprobs`
- Prints `refusal` message, if any

## [1.7.0](https://gitlab.com/ai-gimlab/gptcli/-/releases/v1.7.0) - 2024-07-20

### Fixed

- Error decoding JSON payload when `--model` and `--preview` options are used together
- `--web-select` option not working as expected
- Cannot use a custom system message when using the `--web` option

## [1.6.0](https://gitlab.com/ai-gimlab/gptcli/-/releases/v1.6.0) - 2024-06-20

### Added

- Support for Batch jobs
- File API support
- New API option `parallel_tool_calls: false`
- Better logic for relations between command line options
- Added User-Agent to request header: `gptcli/1.0 (Go-http-client/1.1)`

## [1.5.0](https://gitlab.com/ai-gimlab/gptcli/-/releases/v1.5.0) - 2024-05-26

### Added

- Support for GPT-4 Turbo with Vision.
- Support for GPT-4o.
- Support for function calling API option `tool_choice: "required"`.
- Support for `stream_options: {"include_usage": true}` API stream parameter
  (stream usage stats).

## [1.4.0](https://gitlab.com/ai-gimlab/gptcli/-/releases/v1.4.0) - 2024-01-30

### Added

- Embedding V3 models supported.
- GPT-4 Turbo preview supported.
- New embedding API parameter `dimensions`.
- New moderation model *text-moderation-007*.

## [1.3.0](https://gitlab.com/ai-gimlab/gptcli/-/releases/v1.3.0) - 2024-01-22

### Added

- Embeddings support

## [1.2.0](https://gitlab.com/ai-gimlab/gptcli/-/releases/v1.2.0) - 2024-01-03

### Fixed

- `nil ptr deref` when requesting logprobs in stream mode.

### Changed

- Updated some presets to latest models.

## [1.1.0](https://gitlab.com/ai-gimlab/gptcli/-/releases/v1.1.0) - 2023-12-19

### Added

- Support for `logprobs` and `top_logprobs` param.

## [1.0.0] - 2023-12-12

### First stable release

---

[Back to project main page](https://gitlab.com/ai-gimlab/gptcli#gptcli---overview)

---
