package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"gitlab.com/sysUtils/go-libraries/libhelp"
	"gitlab.com/sysUtils/go-libraries/libopt"

	"github.com/pborman/getopt/v2"
)

func main() {
	var (
		logger            *log.Logger = log.New(os.Stderr, logPrefix, log.Lshortfile)
		responseBody      response
		responseBodyEmbed responseEmbed
		bodyString        string
		payloadJSON       []byte
		errPayload        error
	)

	// --- check user input chars --- //
	// command name
	if err := libopt.ChkChars(chrLowCase+chrUpCase+chrNums+chrCmdName, os.Args[:1]); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// check short/long options for single/double dash
	if len(os.Args) > 1 && os.Args[1] != "" && os.Args[len(os.Args)-1] != "" {
		if err := checkLongOptionDashes(os.Args[1:]); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	}

	// --- check cmdline options --- //
	// any error during parse
	if err := getopt.Getopt(nil); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}
	// check Mutually Exclusive Options
	if err := mutuallyExclusiveOptions(); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}
	// check for flag dependencies
	if err := checkFlagDependencies(); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}
	// check if string arguments was provided
	if err := argProvided(); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// informative flags
	switch {
	case flagHelp:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagHelpLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		libhelp.PrintHelp(helpMsgUsage, helpMsgBody, helpMsgTips, true)
		os.Exit(0)
	case flagVersion:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagVersionLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		libhelp.PrintVersion(verMsg)
		os.Exit(0)
	case flagDefaults:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagDefaultsLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		fmt.Println(defaultValues)
		os.Exit(0)
	case flagEndpointStatus:
		rc, err := checkEndpointStatus()
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		// exit if any endpoint error
		if rc > 0 {
			os.Exit(rc)
		}
		// exit if only status check was requested
		if len(os.Args) == 2 {
			fmt.Printf("endpoint status: OK\n")
			os.Exit(0)
		}
		flagEndpointStatus = false
	case flagListModels:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagListModelsLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		if err := listModels(); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		os.Exit(0)
	case flagListPresets:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagListPresetsLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		var p presetSettings
		p.printList()
		os.Exit(0)
	case flagFunctionExamples:
		if len(os.Args) > 2 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagFunctionExamplesLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		fmt.Printf("%s\n", jsonPayloadExamples)
		os.Exit(0)
	case flagPresetSystem && !flagPreview:
		if len(os.Args) > 5 {
			fmt.Printf("'--%s' requested, ignoring other options. Press ENTER to continue... ", flagPresetSystemLong)
			if _, err := fmt.Scanln(); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Println()
		}
		var p presetSettings
		if err := p.set(flagPreset); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		p.printSysMsg(flagPreset)
		os.Exit(0)
	}

	// check format of 'string' type flags
	if err := checkFlagTypeString(); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// - check non-options arguments: prompt message - //
	nonOptArgs := getopt.Args()
	switch {
	case (len(nonOptArgs) == 0 || (len(nonOptArgs) > 0 && nonOptArgs[0] == "")) && ((flagWeb != "" && flagPreset != "") || flagWebTest || flagPreview || flagTool != "" || flagFileApiUpload != "" || flagBatchCreate != "" || flagBatchStatus != "" || flagBatchList || flagBatchCancel != "" || flagFileApiRetrieve != "" || flagFileApiList || flagFileApiInfo != "" || flagFileApiDelete != ""):
		// when used with presets, web content become the prompt
		prompt = ""
	case len(nonOptArgs) == 0 || (len(nonOptArgs) > 0 && nonOptArgs[0] == ""):
		// no PROMPT given
		logger.Fatal(fmt.Errorf("\a%s\n%s", "missing user PROMPT", tryMsg))
	case len(nonOptArgs) > 1:
		// to many PROMPTs
		var s string
		for i, v := range nonOptArgs {
			if i == 0 {
				s += fmt.Sprintf("[PROMPT %d]\n", i+1)
				s += fmt.Sprintf("%s <- PROBABLY THE SOURCE OF THE PROBLEM\n\n", v)
				continue
			} else {
				s += fmt.Sprintf("[PROMPT %d]\n", i+1)
				s += fmt.Sprintf("%s\n\n", v)
			}
		}
		logger.Fatal(fmt.Errorf("\a%s:\n\n%s\n%s", "too many user PROMPTs or unknown option", s, tryMsg))
	default:
		prompt = nonOptArgs[0]
		// sanitize prompt
		prompt = strings.ReplaceAll(prompt, "\\n", "\n")
	}

	// --- config program --- //
	// parameters
	if err := configFlags(); err != nil {
		switch {
		case err.Error() == "EXIT":
			os.Exit(-1)
		default:
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	}

	// user PROMPT exceptions, if any
	if err := userPromptExceptions(); err != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
	}

	// FileAPI: upload
	if flagFileApiUpload != "" {
		resp, err := httpRequestFileUploadPOST(flagFileApiUpload)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		fmt.Println(resp)
		os.Exit(0)
	}

	// FileAPI: retrieve file content
	if flagFileApiRetrieve != "" {
		// retrieve content
		resp, err := httpRequest(method, url, true)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		// result
		switch {
		case (flagFileApiSave == "" && flagBatchResult == "") || flagPreview:
			// print content to console
			fmt.Println(resp)
		case flagFileApiSave != "" || flagBatchResult != "":
			// save content to local file
			err := writeFile(fileApiSave, []byte(resp))
			if err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Printf("file %q successfully saved\n", fileApiSave)
		}
		os.Exit(0)
	}

	// batch/FileAPI: informations, delete file
	if flagBatchStatus != "" || flagBatchList || flagFileApiList || flagFileApiInfo != "" || flagFileApiDelete != "" {
		resp, err := httpRequest(method, url, true)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		fmt.Println(resp)
		os.Exit(0)
	}

	// batch: cancel batch
	if flagBatchCancel != "" {
		if flagPreview {
			fmt.Printf("[URL]: %s\n", url)
			os.Exit(0)
		}
		resp, err := httpRequestPOST(nil)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		ppJSON, err := prettyPrintJsonString(resp)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		fmt.Println(ppJSON)
		os.Exit(0)
	}

	// '--preview', '--web-test' or '--embed', with '--count', start background counting of input tokens
	// use 'tiktoken-go' library
	if ((flagPreview || flagWebTest) && strings.Contains(flagCount, tokenInput)) || (flagCount != "" && flagPreview && flagEmbed) {
		go countEstimatedInputTokens()
	}

	// if requested, print raw web page, stripped from tags, and exit
	if flagWebTest {
		// print web page
		fmt.Printf("%s\n", webPageContent)
		if flagCount != "" {
			// print input token usage
			printTokensUsage(nil, nil)
		}
		os.Exit(0)
	}

	// config roles
	if !flagEmbed {
		if err := configRoles(); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	}

	// create JSON payload
	payloadJSON, errPayload = createPayloadJSON(prompt)
	if errPayload != nil {
		logger.Fatal(fmt.Errorf("\a%v\n%s", errPayload, tryMsg))
	}

	// preview and exit
	if flagPreview {
		ppJSON, err := prettyPrintJsonString(string(payloadJSON))
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		if flagPreset != "" && flagPresetSystem {
			fmt.Printf("// --- JSON payload --- //\n%s\n\n", ppJSON)
		} else {
			fmt.Printf("%s\n", ppJSON)
		}
		if flagPreset != "" && flagPresetSystem {
			var p presetSettings
			if err := p.set(flagPreset); err != nil {
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
			fmt.Printf("// --- preset %q SYSTEM MESSAGE --- //\n", flagPreset)
			p.printSysMsg(flagPreset)
		}
		if flagCount != "" && strings.Contains(flagCount, tokenInput) {
			printTokensUsage(nil, nil)
		}
		os.Exit(0)
	}

	// batch: save completion to input JSONL file
	if flagBatchPrepare {
		if err := writeFile(batchInputFile, payloadJSON); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		fmt.Printf("batch jobs successfully saved to file %s\n", batchInputFile)
		os.Exit(0)
	}

	// --- execute request and print result --- //
	// make HTTP request (single response mode or stream mode)
	for i := -1; i < retries; i++ {
		resp, err := httpRequestPOST(payloadJSON)
		if err != nil {
			switch {
			case strings.HasSuffix(err.Error(), timeoutErr) && i < (retries-1):
				// manage timeout error and retries
				fmt.Printf("connection timeout (%.0f sec): retry in %d sec. ... attempt %d of %d\n", timeout.Seconds(), retriesWait, i+2, retries)
				time.Sleep(time.Second * time.Duration(retriesWait))
				continue
			default:
				// manage all errors, including 'last timeout retry'
				if i > -1 {
					// print an empty line after all timeout messages, if any
					fmt.Println()
				}
				logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
			}
		}
		// no errors, no timeout: read body, break loop, continue execution
		if i > -1 {
			// print an empty line after all timeout messages, if any
			fmt.Println()
		}
		bodyString = resp
		break
	}

	// batch: batch create results
	if flagBatchCreate != "" {
		fmt.Println(bodyString)
		os.Exit(0)
	}

	// decode JSON response data
	if !flagStream && !flagEmbed {
		if err := json.Unmarshal([]byte(bodyString), &responseBody); err != nil {
			logger.Fatal(fmt.Errorf("\aerror decoding JSON data: %v\n%s", err, tryMsg))
		}
		// count words in normal mode (not stream)
		if strings.Contains(flagCount, outputWords) {
			if len(responseBody.Choices) > 0 {
				for _, s := range responseBody.Choices {
					outputWordsCount += len(strings.Fields(s.Message.Content))
				}
			}
		}
	}

	// decode JSON embedding data
	if flagEmbed && !flagEmbedEncodingFormat {
		if err := json.Unmarshal([]byte(bodyString), &responseBodyEmbed); err != nil {
			logger.Fatal(fmt.Errorf("\aerror decoding JSON data: %v\n%s", err, tryMsg))
		}
	}

	// encode response data to csv and write all data to csv file
	if flagCsv != "" && (!flagModeration && !flagModerationBool && !flagStream && !flagEmbed) {
		responseCSV(responseBody)
		if err := writeFileCSV(newFileCSV); err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
	}

	// --- parse response --- //
	switch {
	case flagStream:
		// dunno - response already printed
	case flagEmbed:
		// save embeddings
		_, err := finalResult(bodyString, nil, &responseBodyEmbed)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		if flagEmbedEncodingFormat {
			os.Exit(0)
		}
	default:
		// print response
		rc, err := finalResult(bodyString, &responseBody, nil)
		if err != nil {
			logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
		}
		if flagModeration || flagModerationBool {
			os.Exit(rc)
		}
		// if requested print logprobs
		if len(responseBody.Choices) > 0 {
			finishReason := responseBody.Choices[0].FinishReason
			if flagLogProbs && (!flagJson && !flagRaw && finishReason != "tool_calls") {
				err := printLogprobs(bodyString, &responseBody)
				if err != nil {
					logger.Fatal(fmt.Errorf("\a%v\n%s", err, tryMsg))
				}
			}
		}
	}

	// prints how many tokens and/or words have been used
	if flagCount != "" {
		fmt.Println()
		switch {
		case flagEmbed:
			printTokensUsage(nil, &responseBodyEmbed)
		case flagStream:
			var u usage
			var r response
			u.PromptTokens = streamTokens[inputTokens]
			u.CompletionTokens = streamTokens[outputTokens]
			u.TotalTokens = streamTokens[totalTokens]
			r.Usage = &u
			printTokensUsage(&r, nil)
		default:
			printTokensUsage(&responseBody, nil)
		}
	}
}
