package main

import (
	"bufio"
	"context"
	"encoding/base64"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"math"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	tiktoken "github.com/pkoukk/tiktoken-go"
)

// check if user given model name is a valid model name
func checkModelName(name string) error {
	// function name to return if any error occur
	var funcName string = "checkModelName"

	if err := listModels(); err != nil {
		return err
	}

	// check against registered names
	for _, modelName := range modelNames {
		if modelName == name {
			return nil
		}
	}

	// if not found, return error
	return fmt.Errorf("func %q - wrong model name: %q", funcName, name)
}

// count input tokens in preview mode and web-test mode
func countEstimatedInputTokens() {
	// function name to return if any error occur
	var funcName string = "countEstimatedInputTokens"

	// create 'tiktoken' encoder
	tke, err := tiktoken.GetEncoding(tokensEncoding)
	if err != nil {
		err = fmt.Errorf("getEncoding: %v", err)
		fmt.Printf("%v\n", fmt.Errorf("func %q - %w\n", funcName, err))
		return
	}

	// calculate offset (very empirically)
	var offset int = defaultInputMessageOffset
	if flagSystem != "" || flagPreview || flagPreset != "" || flagWeb != "" || flagAssistant != "" {
		offset += defaultInSysAssistMsgOffset
	}

	// system PROMPT
	var systemPrompt string
	switch {
	case flagWebTest:
		// estimate only web page raw token usage
		estimatedInputTokens = len(tke.Encode(webPageContent, nil, nil))
		return
	case flagEmbed:
		var b strings.Builder
		var prompts []string = strings.Split(prompt, ",,")
		for _, p := range prompts {
			b.WriteString(p)
		}
		estimatedInputTokens = len(tke.Encode(b.String(), nil, nil))
		return
	case flagSystem != "":
		systemPrompt = flagSystem
	case flagPreset != "":
		systemPrompt = presets[flagPreset].systemMessage
	case flagWeb != "":
		systemPrompt = systemMessageWeb
	}

	// result
	estimatedInputTokens = len(tke.Encode(prompt+systemPrompt+flagAssistant, nil, nil)) + offset
}

// tokens usage
func printTokensUsage(r *response, re *responseEmbed) {
	// function name to return if any error occur
	var funcName string = "printTokensUsage"

	// Set a read timeout for 'tiktoken'
	ctx, cancel := context.WithTimeout(context.Background(), timeoutTiktoken)
	defer cancel()

	switch {
	case (flagPreview && flagEmbed) || flagPreview || flagWebTest:
		// estimated input tokens
		if !strings.Contains(flagCount, tokenInput) && !flagEmbed {
			fmt.Printf("\a\n'--%s' or '--%s' and '-%c' or '--%s' provided: %q tokens not available\n", flagPreviewLong, flagWebTestLong, flagCountShort, flagCountLong, flagCount)
			return
		}
		for ctxDone := true; ctxDone; {
			select {
			case <-ctx.Done():
				fmt.Printf("func %q - timeout counting input tokens\n", funcName)
				ctxDone = false
			default:
				if estimatedInputTokens > 0 {
					if flagWebTest {
						fmt.Printf("\nestimated web page tokens: %d\n", estimatedInputTokens)
					} else {
						fmt.Printf("\nestimated input tokens: %d\n", estimatedInputTokens)
					}
					ctxDone = false
				}
			}
		}
	case flagEmbed:
		// embeddings
		fmt.Printf("input tokens:\t%d\n", re.Usage.PromptTokens)
	case flagCount == tokenInput:
		// single argument (any of 'in', 'out', 'total', 'words')
		fmt.Printf("%d\n", r.Usage.PromptTokens)
	case flagCount == tokenOutput:
		fmt.Printf("%d\n", r.Usage.CompletionTokens)
	case flagCount == tokenSumInOut:
		fmt.Printf("%d\n", r.Usage.TotalTokens)
	case flagCount == outputWords:
		fmt.Printf("%d\n", outputWordsCount)
	case strings.Contains(flagCount, ","):
		// --- multiple arguments (a combination of 'in', 'out', 'total', 'words')
		if strings.Contains(flagCount, tokenInput) {
			fmt.Printf("input tokens:\t%d\n", r.Usage.PromptTokens)
		}
		if strings.Contains(flagCount, tokenOutput) {
			fmt.Printf("output tokens:\t%d\n", r.Usage.CompletionTokens)
		}
		if strings.Contains(flagCount, tokenSumInOut) {
			fmt.Printf("total tokens:\t%d\n", r.Usage.TotalTokens)
		}
		if strings.Contains(flagCount, outputWords) {
			fmt.Printf("output words:\t%d\n", outputWordsCount)
		}
	}
}

// export csv data
func writeFileCSV(newFile bool) error {
	// function name to return if any error occur
	var funcName string = "writeFileCSV"

	// create or open file (append)
	f, err := os.OpenFile(flagCsv, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return fmt.Errorf("func %q - file %s: %w\n", funcName, flagCsv, err)
	}
	defer f.Close()

	// check file size
	fileInfo, err := f.Stat()
	if err != nil {
		return fmt.Errorf("func %q - error checking csv file size: %w\n", funcName, err)
	}

	// prepare data
	var b strings.Builder
	if newFile || fileInfo.Size() == 0 {
		// if csv file does not exist, or is empty, write headers
		if _, err := b.WriteString(csvFieldsName); err != nil {
			return fmt.Errorf("func %q - error composing csv [header]: %w\n", funcName, err)
		}
	}

	// append row
	for i := 0; i < len(csvFields); i++ {
		if _, err := b.WriteString(csvFields[i]); err != nil {
			return fmt.Errorf("func %q - error composing csv [data]: %w\n", funcName, err)
		}
		if i < len(csvFields)-1 {
			if _, err := b.WriteString(","); err != nil {
				return fmt.Errorf("func %q - error composing csv [data]: %w\n", funcName, err)
			}
		}
	}

	// end row with newline
	if _, err := b.WriteString("\n"); err != nil {
		return fmt.Errorf("func %q - error composing csv [data]: %w\n", funcName, err)
	}

	// write data to csv file
	if _, err := f.Write([]byte(b.String())); err != nil {
		return fmt.Errorf("func %q - file %s: %w\n", funcName, flagCsv, err)
	}

	return nil
}

// encode request data to csv
func requestCSV(req *request) {
	// user, system, assistant PROMPTs
	if len(requestMessages) > 0 {
		for _, m := range requestMessages {
			// sanitize message: all in a single line
			s := m.Content
			s = strings.ReplaceAll(s, "\n", "\\n")
			switch {
			case m.Role == system:
				csvFields[requestContentSystem] = "\"" + s + "\""
			case m.Role == user:
				csvFields[requestContentUser] = "\"" + s + "\""
			}
		}
	}

	// request parameters
	csvFields[requestModel] = req.Model
	csvFields[requestResponseFormat] = req.ResponseFormat.Type
	csvFields[requestTemperature] = fmt.Sprintf("%.2f", req.Temperature)
	csvFields[requestTop_p] = fmt.Sprintf("%.2f", req.TopP)
	csvFields[requestPresencePenalty] = fmt.Sprintf("%.2f", req.PresencePenalty)
	csvFields[requestFrequencyPenalty] = fmt.Sprintf("%.2f", req.FrequencyPenalty)
	switch {
	case req.Seed == nil:
		csvFields[requestSeed] = fmt.Sprintf("null")
	default:
		csvFields[requestSeed] = fmt.Sprintf("%d", req.Seed)
	}

	// format logit_bias object as oneline string
	if flagLogitBias != "" {
		s := flagLogitBias
		// sanitize string
		s = strings.TrimSpace(s)
		// add logit bias to csv
		s = strings.Trim(s, "{}")
		s = strings.ReplaceAll(s, "\"", "")
		s = "\"" + s + "\""
		csvFields[requestLogitBias] = s
	}

	if flagFunction != "" {
		csvFields[requestFunction] = "true"
		csvFields[requestFunctionCall] = functionCall
	}
}

// encode response data to csv
func responseCSV(resp response) {
	csvFields[responseModel] = resp.Model
	csvFields[responseInputTokens] = fmt.Sprintf("%d", resp.Usage.PromptTokens)
	csvFields[responseOutputTokens] = fmt.Sprintf("%d", resp.Usage.CompletionTokens)
	csvFields[responseTotalTokens] = fmt.Sprintf("%d", resp.Usage.TotalTokens)
	csvFields[responseOutputWords] = fmt.Sprintf("%d", outputWordsCount)
	csvFields[responseSystemFingerprint] = resp.SystemFingerprint

	switch {
	case flagFunction != "" && resp.Choices[0].FinishReason == functionCallFinishReason:
		fname := make([]string, 0)
		fargs := make([]string, 0)
		// function call YES
		for i, t := range resp.Choices[0].Message.ToolCalls {
			if t.Function.Arguments != "{}" {
				item := fmt.Sprintf("[%d]:", i)
				// this field contain commas and double quotes, but quotes are needed to delimit field, so they are removed
				args := strings.ReplaceAll(t.Function.Arguments, "\"", "")
				fname = append(fname, item+"("+t.Function.Name+")")
				fargs = append(fargs, item+args)
			}
		}
		csvFields[responseFunctionCall] = "true"
		csvFields[responseFunctionName] = strings.Join(fname, "; ")
		csvFields[responseFunctionArgs] = "\"" + strings.Join(fargs, "; ") + "\""
	case flagFunction != "" && resp.Choices[0].FinishReason != functionCallFinishReason:
		// function call NO
		csvFields[responseFunctionCall] = "false"
	}

	// sanitize response message: all in a single line, remove extra quotes
	s := resp.Choices[0].Message.Content
	s = strings.ReplaceAll(s, "\n", "\\n")
	s = strings.ReplaceAll(s, "\"", "")
	csvFields[responseContent] = "\"" + s + "\""
}

func embeddingsCSV(re *responseEmbed) error {
	// function name to return if any error occur
	var funcName string = "embeddingsCSV"

	// last row id of existing csv file - used for append data
	var embedCsvFileOffset int = 0

	// use custom csv file
	if flagCsv != "" {
		embedCsvFile = flagCsv
	}

	// create or open file (append)
	f, err := os.OpenFile(embedCsvFile, os.O_APPEND|os.O_CREATE|os.O_RDWR, 0600)
	if err != nil {
		return fmt.Errorf("func %q - error creating embeddings csv file: %w\n", funcName, err)
	}
	defer f.Close()

	// check file size
	fileInfo, err := f.Stat()
	if err != nil {
		return fmt.Errorf("func %q - error checking csv file size: %w\n", funcName, err)
	}

	// Create a CSV writer
	csvWriter := csv.NewWriter(f)

	// Create a CSV reader
	csvReader := csv.NewReader(f)

	// if new file: write headers
	// else: check last row id
	switch {
	case fileInfo.Size() == 0:
		// if csv file does not exist, or is empty, write headers
		if err := csvWriter.Write(embedCsvHeader); err != nil {
			return fmt.Errorf("func %q - error writing headers to csv file %s: %w\n", funcName, embedCsvFile, err)
		}
	case fileInfo.Size() != 0:
		var records [][]string
		for {
			record, err := csvReader.Read()
			if err == io.EOF {
				break
			}
			if err != nil {
				return fmt.Errorf("func %q - error reading csv file %s: %w\n", funcName, embedCsvFile, err)
			}

			records = append(records, record)
		}
		if offset, err := strconv.ParseInt(records[len(records)-1][0], 10, 64); err != nil {
			return fmt.Errorf("func %q - error reading last line of csv file %s: %w\n", funcName, embedCsvFile, err)
		} else {
			embedCsvFileOffset = int(offset) + 1
		}
	}

	for i, v := range re.Data {
		var embedding strings.Builder
		id := fmt.Sprintf("%d", embedCsvFileOffset)
		text := inputs[i]
		if _, err := embedding.WriteString("["); err != nil {
			return fmt.Errorf("func %q - error creating embeddings CSV row: %w\n", funcName, err)
		}
		for i, e := range v.Embedding {
			f := strconv.FormatFloat(e, 'f', -1, 64)
			trimmed := strings.TrimRight(strings.TrimRight(f, "0"), ".")
			if _, err := embedding.WriteString(trimmed); err != nil {
				return fmt.Errorf("func %q - error creating embeddings CSV field: %w\n", funcName, err)
			}
			if i < len(v.Embedding)-1 {
				if _, err := embedding.WriteRune(','); err != nil {
					return fmt.Errorf("func %q - error creating embeddings CSV field: %w\n", funcName, err)
				}
			}
		}
		if _, err := embedding.WriteRune(']'); err != nil {
			return fmt.Errorf("func %q - error creating embeddings CSV row: %w\n", funcName, err)
		}

		// Write the data record to the CSV file.
		if err := csvWriter.Write([]string{id, text, embedding.String()}); err != nil {
			return fmt.Errorf("func %q - error writing data to CSV file %s: %w\n", funcName, embedCsvFile, err)
		}
		embedCsvFileOffset++
	}

	// Flush the CSV writer.
	csvWriter.Flush()

	fmt.Printf("\nembeddings data successfully written to file %q\n\n", embedCsvFile)

	return nil
}

func finalResult(bodyString string, r *response, re *responseEmbed) (rc int, err error) {
	// --- parse response --- //
	switch {
	case ((flagModeration || flagModerationBool) || flagEmbed) && (flagJson || flagRaw || flagEmbedEncodingFormat):
		// - full JSON moderation report - //
		// raw/one line JSON
		if flagRaw {
			fmt.Printf("%s\n", bodyString)
		}
		// format one line JSON
		if flagJson {
			result, err := prettyPrintJsonString(bodyString)
			if err != nil {
				return 1, err
			}
			fmt.Printf("%s\n", result)
		}
		return 0, nil
	case flagModeration || flagModerationBool:
		// - simple moderation report (safe, not safe or false, true) - //
		result, moderationModelUsed, err := extractModerationResponse(bodyString)
		if err != nil {
			return 1, err
		}
		if !result {
			if flagModerationBool {
				fmt.Printf("false\n")
			} else {
				fmt.Printf("safe\n")
			}
			if flagModelUsed {
				fmt.Printf("\n%s\n", moderationModelUsed)
			}
			return 0, nil
		} else {
			if flagModerationBool {
				fmt.Printf("true\n")
			} else {
				fmt.Printf("not safe\n")
			}
			if flagModelUsed {
				fmt.Printf("\n%s\n", moderationModelUsed)
			}
			return 125, nil
		}
	case flagEmbed:
		// - embedding - //
		if err := embeddingsCSV(re); err != nil {
			return 0, err
		}
		return 0, nil
	case !flagStream:
		// - NOT stream mode - //
		// print raw or json response (in single response mode raw response is already json formatted)
		if flagRaw || flagJson {
			if flagVision != "" && flagJson {
				ppJSON, err := prettyPrintJsonString(bodyString)
				if err != nil {
					return 1, err
				}
				fmt.Printf("%s", ppJSON)
				return 0, nil
			}
			fmt.Printf("%s", bodyString)
			return 0, nil
		}
		// print results
		if len(r.Choices) > 0 {
			for i, v := range r.Choices {
				// check for refusal message
				if v.Message.Refusal != "" {
					fmt.Printf("[MODEL REFUSAL MESSAGE]: %q\n\n", v.Message.Refusal)
					continue
				}
				if len(r.Choices) > 1 {
					// multiple responses requested
					fmt.Printf("[RESPONSE %d]\n", i+1)
					fmt.Printf("%s\n\n", v.Message.Content)
					continue
				}
				// function call YES
				if flagFunction != "" && (v.FinishReason == functionCallFinishReason || v.Message.ToolCalls != nil) {
					fmt.Printf("function call: yes\n\n")
					// print function results
					for i, t := range v.Message.ToolCalls {
						fmt.Printf("function name: %s\n", t.Function.Name)
						fmt.Printf("function id:   %s\n", t.Id)
						if t.Function.Arguments != "{}" {
							fmt.Printf("function args: %s\n", t.Function.Arguments)
							if i < len(v.Message.ToolCalls)-1 {
								fmt.Println()
							}
						}
					}
					continue
				} else if flagFunction != "" && v.FinishReason != functionCallFinishReason {
					// function call NO
					fmt.Printf("function call: no\n\n")
					fmt.Printf("%s\n", v.Message.Content)
					continue
				} else {
					// single response
					if flagLogProbs {
						fmt.Println("[RESPONSE]:")
					} else {
						// normal response
						fmt.Printf("%s\n", v.Message.Content)
					}
				}
			}
			// print model name and/or fingerprint, if requested
			if flagModelUsed {
				fmt.Printf("\n%s\n", r.Model)
			}
			if flagFingerprint {
				fmt.Printf("\n%s\n", r.SystemFingerprint)
			}
		}
	}
	return 0, nil
}

func printLogprobs(bodyString string, response *response) error {
	if len(response.Choices) > 0 && response.Choices[0].Logprobs != nil && response.Choices[0].Logprobs.Content != nil && len(response.Choices[0].Logprobs.Content) > 0 {
		lenRow := 120
		switch {
		case topLogProbs == 0:
			fmt.Println()
			fmt.Println("[LOGPROBS]:")
			for i := 0; i < lenRow; i++ {
				fmt.Printf("-")
			}
			fmt.Println()
			fmt.Printf("%-20s%-24s%14s\t\t%s\n", "token", "logprob", "%", "bytes")
			for i := 0; i < lenRow; i++ {
				fmt.Printf("-")
			}
			fmt.Println()
			for i := range response.Choices[0].Logprobs.Content {
				percent := math.Exp(response.Choices[0].Logprobs.Content[i].Logprob) * 100
				fmt.Printf("%-20s", response.Choices[0].Logprobs.Content[i].Token)
				fmt.Printf("%-24g", response.Choices[0].Logprobs.Content[i].Logprob)
				fmt.Printf("%14.8f\t\t", percent)
				fmt.Printf("%v", response.Choices[0].Logprobs.Content[i].Bytes)
				fmt.Println()
			}
			return nil
		default:
			fmt.Println()
			fmt.Println("[TOP_LOGPROBS]:")
			for i := 0; i < lenRow; i++ {
				fmt.Printf("-")
			}
			fmt.Println()
			fmt.Printf("%-20s%-24s%14s\t\t%s\n", "token", "logprob", "%", "bytes")
			for i := 0; i < lenRow; i++ {
				fmt.Printf("-")
			}
			fmt.Println()
			for i := range response.Choices[0].Logprobs.Content {
				for p := range response.Choices[0].Logprobs.Content[i].TopLogprobs {
					percent := math.Exp(response.Choices[0].Logprobs.Content[i].TopLogprobs[p].Logprob) * 100
					fmt.Printf("%-20s", response.Choices[0].Logprobs.Content[i].TopLogprobs[p].Token)
					fmt.Printf("%-24g", response.Choices[0].Logprobs.Content[i].TopLogprobs[p].Logprob)
					fmt.Printf("%14.8f\t\t", percent)
					fmt.Printf("%v", response.Choices[0].Logprobs.Content[i].TopLogprobs[p].Bytes)
					fmt.Println()
				}
				for i := 0; i < lenRow; i++ {
					fmt.Printf("-")
				}
				fmt.Println()
			}
			return nil
		}
	}
	return fmt.Errorf("%q or %q informations not available\n", "logprobs", "top_logprobs")
}

/*
func printLogprobs(bodyString string, response *response) error {
	if len(response.Choices) > 0 && response.Choices[0].Logprobs != nil && response.Choices[0].Logprobs.Content != nil && len(response.Choices[0].Logprobs.Content) > 0 {
		lenRow := 120
		fmt.Println()
		fmt.Println("[TOP_LOGPROBS]:")
		for i := 0; i < lenRow; i++ {
			fmt.Printf("-")
		}
		fmt.Println()
		fmt.Printf("%-20s%-24s%14s\t\t%s\n", "token", "logprob", "%", "bytes")
		for i := 0; i < lenRow; i++ {
			fmt.Printf("-")
		}
		fmt.Println()
		for i := range response.Choices[0].Logprobs.Content {
			for p := range response.Choices[0].Logprobs.Content[i].TopLogprobs {
				percent := math.Exp(response.Choices[0].Logprobs.Content[i].TopLogprobs[p].Logprob) * 100
				fmt.Printf("%-20s", response.Choices[0].Logprobs.Content[i].TopLogprobs[p].Token)
				fmt.Printf("%-24g", response.Choices[0].Logprobs.Content[i].TopLogprobs[p].Logprob)
				fmt.Printf("%14.8f\t\t", percent)
				fmt.Printf("%v", response.Choices[0].Logprobs.Content[i].TopLogprobs[p].Bytes)
				fmt.Println()
			}
			for i := 0; i < lenRow; i++ {
				fmt.Printf("-")
			}
			fmt.Println()
		}
		return nil
	}
	return fmt.Errorf("%q or %q informations not available\n", "logprobs", "top_logprobs")
}
*/

func checkEndpointStatus() (rc int, err error) {
	// function name to return if any error occur
	var funcName string = "checkEndpointStatus"

	var e endpointStatus
	r, err := httpRequest(method, urlEndpointStatus, false)
	if err != nil {
		return 1, fmt.Errorf("func %q - error requesting endpoint status: %w\n", funcName, err)
	}
	if err := json.Unmarshal([]byte(r), &e); err != nil {
		return 1, fmt.Errorf("func %q - error decoding json data: %w\n", funcName, err)
	}
	if len(e.Components) > 0 {
		switch e.Components[0].Status {
		case "operational":
			return 0, nil
		default:
			fmt.Fprintf(os.Stderr, "endpoint status: experiencing issues\n\ndetails:\n")
			fmt.Fprintf(os.Stderr, "  name: %s\n", e.Components[0].Name)
			fmt.Fprintf(os.Stderr, "  status: %s\n", e.Components[0].Status)
			fmt.Fprintf(os.Stderr, "  description: %s\n", e.Components[0].Description)
			fmt.Fprintf(os.Stderr, "  updated_at: %s\n", e.Components[0].UpdatedAt)
			fmt.Fprintf(os.Stderr, "  id: %s\n", e.Components[0].Id)
			fmt.Fprintf(os.Stderr, "  page_id: %s\n", e.Components[0].PageId)
			return 125, nil
		}
	}
	return 0, nil
}

func validateVision(visionImagesArgs []string) error {
	// function name to return if any error occur
	var funcName string = "validateVision"

	if len(visionImagesArgs) > 0 {
		for _, v := range visionImagesArgs {
			switch {
			case strings.HasPrefix(v, "http"):
				visionImagesURLs = append(visionImagesURLs, v)
			default:
				f, err := os.ReadFile(filepath.Clean(v))
				if err != nil {
					return fmt.Errorf("func %q - file %s: %w\n", funcName, v, err)
				}
				imageFileBase64Encoded := base64.StdEncoding.EncodeToString(f)
				visionImagesLocal = append(visionImagesLocal, imageFileBase64Encoded)
			}
		}
	}
	return nil
}

// count batch jsonl lines
func countFileLines(filename string) (lines int, err error) {
	// function name to return if any error occur
	var funcName string = "countFileLines"

	f, err := os.Open(filepath.Clean(filename))
	if err != nil {
		return 0, fmt.Errorf("func %q - error opening file %s: %w\n", funcName, filename, err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	lines = 0

	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		lines++
	}

	if err := scanner.Err(); err != nil {
		return 0, fmt.Errorf("func %q - error reading file %s: %w\n", funcName, f.Name(), err)
	}

	return lines, nil
}

// write data, such as single completion to JSONL input batch file, to file
func writeFile(filename string, data []byte) error {
	// function name to return if any error occur
	var funcName string = "writeFile"

	// create or open file (append)
	f, err := os.OpenFile(filepath.Clean(filename), os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return fmt.Errorf("func %q - file %s: %w\n", funcName, batchInputFile, err)
	}
	defer f.Close()

	// write data to jsonl file
	data = append(data, '\n')
	if _, err := f.Write(data); err != nil {
		return fmt.Errorf("func %q - file %s: %w\n", funcName, filename, err)
	}

	return nil
}
