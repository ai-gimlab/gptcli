package main

const helpMsgUsage = "Usage: %s [OPTIONS] \"PROMPT\"\n"

const helpMsgBody = `
Terminal Chat Completion client for OpenAI's models

Defaults:
  model: gpt-4o-mini
  model embeddings: text-embedding-3-small
  model vision: gpt-4o
  model moderation: text-moderation-latest
  function call if no functions are required: none
  function call if functions are required: auto
  parallel function calls: true
  temperature: 0.0
  top_p: 1.0
  presence_penalty: 0.0
  frequency_penalty: 0.0
  seed: null
  vision detail: auto
  max response tokens: 1000
  max text response tokens for vision: 300
  number of responses: 1
  response format: text
  stream response mode: false
  connection timeout: 120 sec
  connection timeout stream mode (chunk, total): 5 sec, 180 sec
  number of retries: 0
  wait before retry: 10 sec
  logprobs: false
  top logprobs: 0
  embeddings csv filename: data.csv
  embeddings encoding format: float
  batch input filename: batch_input.jsonl
  batch url: selected based on the task, either completions or embeddings
  batch (prefix) id: request
  batch endpoint: /v1/chat/completions
  batch list limit: 20

Order matter:
  first OPTIONS
  then PROMPT

Notes:
  . PROMPT must always be enclosed in "double" quotes.
  . When using [-m, --model] option it is possible that the endpoint
    use an equivalent, but more capable and up-to-date model.
    You can check which model was used with option '--model-used'

Online Documentation: <https://gitlab.com/ai-gimlab/gptcli#gptcli---overview>

OPTIONS:
Global:
      --defaults                  prints the program default values
  -l, --list-models               list names of all available models and exit
  -m, --model=MODEL               select model
                                  use '--list-models' to list all available models
  -p, --preview                   preview request payload and exit
      --response-raw              print full response body raw
      --response-json             print full response body json formatted
      --help                      print this help
      --version                   output version information and exit

Network:
      --retries=RETRIES           number of connection retries if network timeout occur
                                  use the '--retries-wait' option to insert a pause between retries
      --retries-wait=SECONDS      in conjunction with '--retries RETRIES' option insert a pause between retries
      --status                    check endpoint status before sending any request
                                  exit program if any issue occur at endpoint side
      --timeout=SECONDS           network connection timeout, in seconds
                                  apply to a complete request/response session
                                  SECONDS = 0 means no timeout
      --timeout-chunk=SECONDS     stream mode network connection timeout, in seconds
			          apply to every streamed chunk of response

API Auth:                         ref: <https://gitlab.com/ai-gimlab/gptcli#how-to-get-and-use-openai-api-key>
  -f, --file=APIKEY_FILE          file with Openai api key (OPENAI_API_KEY=your_apikey)

Chat/Completion/Vision API:       ref: <https://platform.openai.com/docs/api-reference/chat>
  -a, --assistant="PROMPT"        [role]: 'assistant' message PROMPT
                                  must be used with '--previous-prompt' option
  -c, --count=SELECT              prints how many tokens have been used
                                  can also print the word count of the AI response
                                  SELECT can be 'in', 'out', 'total' (in + out) or 'words'
			          SELECT can also be any comma separated combination of above,
			          without any space, without trailing comma:
 			            in,out
			            in,out,total,words
			            etc...
			          with options '--moderation' or '--moderation-bool' it has no effect
      --csv=CSV_FILE              export request/response data, csv formatted, to CSV_FILE
			          no stream mode, no multiple response, no moderation
                                   - if CSV_FILE does not exist: ask permission to create a new one
			           - if CSV_FILE exist: append data
      --fingerprint               print the system fingerprint of model backend configuration
      --fp=VALUE                  frequency penalty: VALUE range from -2.0 to 2.0
      --format=FORMAT             the format that the model must output
                                  FORMAT can be either 'text' or 'json_object' (or simply 'json')
      --function='{JSON}'         json object containing function definition
			          must be enclosed in single quotes
			          no stream mode
			          use '--function-examples' to view how to compose json object
                                  support multiple functions
				  ref: <https://gitlab.com/ai-gimlab/gptcli/-/tree/main/examples?ref_type=heads#functions>
      --function-call="MODE"      how the model responds to function calls
                                  MODE can be:
			           - "auto" (model choose if calling function)
		         	   - "none" (model does not call a function)
				   - "required" (model is forced to call one or more functions)
				   - "FUNCTION_NAME" (always call function "FUNCTION_NAME")
			          MODE must always be enclosed in single or double quotes
      --function-examples         print some examples of function json object and exit
      --function-noparallel       disable parallel function calling
      --list-presets              list all available predefined tasks and exit
      --lb='{"ID": BIAS}'         logit bias: BIAS range from -100 to 100
                                  json object: maps token IDs to a BIAS value
			          must be enclosed in single quotes
			          keys must always be enclosed in double quotes
			          eg: --lb '{"1234": -10, "4567": 20, "890": -90}'
      --logprobs                  return log probabilities of the output tokens in tabular format
                                   - in conjunction with the '--response-json' or '--response-raw' options
				     logprobs are included in the response, json formatted
				   - with '--stream' option logprobs are printed only if used
				     in conjunction with the '--response-json' or '--response-raw' options
      --model-used                which model was used at endpoint side
      --name=NAME                 NAME of the author of the request
      --pp=VALUE                  presence penalty: VALUE range from -2.0 to 2.0
      --preset=PRESET_NAME        predefined tasks, such as summarization, sentiment analisys, etc...
                                  use '--list-presets' to list all available PRESET_NAMEs and their purpose
      --preset-system             print PRESET_NAME predefined system message and exit
      --previous-prompt=PROMPT    in conjunction with the '--assistant' option simulates a single round of chat
                                  in conjunction with the '--tool' option simulates a single round of function call
  -r, --response-tokens=TOKENS    maximun number of response tokens
                                  range from 0 to model max tokens minus input tokens
			          ref: <https://platform.openai.com/docs/models> for models context size
      --responses=NUMBER          how many responses to generate for each input message
                                  no multiple responses in stream mode, moderation mode and csv export
      --seed=NUMBER               integer number
                                  multiple requests with same prompt, seed and params should return similar/equal result
      --stop="STOP,..."           comma separated list of stop sequences, up to 4 sequences,
			          without trailing comma, enclosed in single or double quotes
			          eg: "STOP1,STOP2,..."
      --stream		          mimic ChatGPT behavior
                                  start printing completion before the full completion is finished
  -s, --system="PROMPT"           [role]: system message PROMPT
  -t, --temperature=VALUE         VALUE range from 0.0 to 2.0
      --tool='[{JSON}]'           json object containing model response to a 'tool call', such as function call
                                  must be used in conjunction with '--previous-prompt' option
			          must begin and end with square bracket and enclosed in single quotes
				  ref: <https://gitlab.com/ai-gimlab/gptcli/-/tree/main/examples?ref_type=heads#tools>
      --tool-content='{JSON}'     json object containing result from user application function
			          must be enclosed in single quotes
				  ref: <https://gitlab.com/ai-gimlab/gptcli/-/tree/main/examples?ref_type=heads#tools>
      --top-logprobs=NUMBER       number of most likely tokens to return at each token position
                                  NUMBER is an integer between 0 and 20
				  must be used in conjunction with the '--logprobs' option
                                   - in conjunction with the '--response-json' or '--response-raw' options
				     logprobs are included in the response, json formatted
				   - with '--stream' option logprobs are printed only if used
				     in conjunction with the '--response-json' or '--response-raw' options
      --top-p=VALUE               top_p: VALUE range from 0.0 to 1.0
      --user=USER                 USER unique identifier representing your end-user
                                  useful in organizations, for any end-user policy violations
  -v, --vision=IMAGE_FILE         answer questions about IMAGE_FILE
                                  IMAGE_FILE can be a local file or an url link to file
			          no tool and web option if vision is requested
      --vision-detail=DETAIL      vision resolution of image processing
                                  DETAIL can be 'low' (disable high res) or 'high' (enable high res)
      --web=URL                   any web page URL (dynamic web pages are not supported)
			          URL must always be encloded in single or double quotes
			          in user PROMPT ask only for the context of the page. Eg.:
			           - WRONG: gptcli --web "https://www.example.com" "From the following web page {QUESTION}"
			           - OK:    gptcli --web "https://www.example.com" "Extract a list of cat names"
			          used in conjunction with option '--preset', URL content becomes the user PROMPT. Eg:
			           - gptcli --preset ner --web "https://www.example.com"
			             execute NER preset using content from "https://www.example.com" web page
      --web-select=SELECTOR       select a section of web page, then strip html tags
                                  SELECTOR can be any HTML element, such as 'h2', 'p', 'body', 'article', etc...
                                  SELECTOR can also be any class or id attribute (eg.: '.myClass', '#myID')
			          lastly SELECTOR can be 'NONE' keyword: no html tag stripping
			           - Note: NONE can use a lot of tokens
			          SELECTOR must always be encloded in single or double quotes
			          default selectors are 'main', if exist, or 'body'
      --web-test                  print an output of text extracted from web page and exit
                                  useful to find the right HTML selector (check --web-select option)
			          with the right SELECTOR less tokens are used

Embeddings API:                   ref: <https://platform.openai.com/docs/api-reference/embeddings>
  -e, --embed                     return PROMPT embedding
                                  PROMPT must always be enclosed in "double" quotes
				  multiple PROMPTs must be separated by double commas
				  eg: gptcli --embed "PROMPT_1,,PROMPT_2,,...,,PROMPT_N"
				  return a cvs table with columns 'id, text, embedding'
				  append table to file 'data.csv' or create new one
				   - to save in a different file use '--csv FILENAME' option
				   - in conjunction with the '--response-json' or '--response-raw' options
				     response is not saved to file, instead output json data to stdout
      --embed-dimensions=VALUE    change embedding vector size to VALUE dimensions
      --encoding-format           set embedding encoding format to base64 (default: float)
                                  response is not saved to file, instead output json data to stdout

Moderation API:                   ref: <https://platform.openai.com/docs/api-reference/moderations>
      --moderation                use 'moderation' endpoint: [safe/not safe] result
                                  identify content that OpenAI's policies prohibits
			          safe: compliant with OpenAI's policies (return code: 0)
			          not safe: violate OpenAI's policies (return code: 125)
			          use option '--response-json' for full report
      --moderation-bool           equivalent to '--moderation', but with [true/false] result
			          false: compliant with OpenAI's policies (return code: 0)
			          true: violate OpenAI's policies (return code: 125)

Batch API:                        ref: <https://platform.openai.com/docs/guides/batch/batch-api>
  PREPARE BATCH:
      --batch-id=PREFIX           prefix for unique custom id value to reference results after completion
				  a sequential number is appended to PREFIX:
				   - PREFIX = 'myrequest'
				     resulting custom_id are 'myrequest-1, myrequest-2, ..., myrequest-n'
                                  it is used in conjuction with '--batch-prepare' option
      --batch-input=JSONL_FILE    batch input file containing requests created with '--batch-prepare' option
                                   - if JSONL_FILE does not exist: ask permission to create a new one
			           - if JSONL_FILE exist: append data
      --batch-prepare             create a '.jsonl' file with batch requests
                                  default JSONL filename: 'batch_input.jsonl' (append mode)
				  use '--batch-input=JSONL_FILE' option to specify your own JSONL file
  CREATE BATCH:
      --batch-create=FILE_ID      create a new batch
                                  FILE_ID is the ID of a JSONL file uploaded with the '--fupload=FILENAME' option
      --batch-endpoint=ENDPOINT   endpoint to be used for all requests in the batch
                                  ENDPOINT can be one of '/v1/chat/completions' or '/v1/embeddings'
                                  it is used in conjuction with '--batch-create=FILE_ID' option
      --batch-metadata='{JSON}'   json object containing custom metadata for the batch
			          must be enclosed in single quotes
                                  it is used in conjuction with '--batch-create=FILE_ID' option
  INFO:
      --batch-list                list batches
      --batch-listlimit=LIMIT     limit the number of returned batches from '--batch-list' option
      --batch-listafter=BATCH_ID  from '--batch-list' option return only batches after BATCH_ID
      --batch-status=BATCH_ID     status check of BATCH_ID created previously with '--batch-create=FILE_ID' option
  CANCEL BATCH:
      --batch-cancel=BATCH_ID     cancels an in-progress BATCH_ID
  SAVE BATCH RESULTS:
      --batch-result=JSONL_FILE   JSONL_FILE where to save the results when batch is complete
				  equivalent to '--fsave=FILENAME' File API option
                                  must bu used in conjuction with '--fretrieve=OUTPUT_FILE_ID'
				  OUTPUT_FILE_ID is the 'output_file_id' field of the json batch object

File API:                         ref: <https://platform.openai.com/docs/api-reference/files>
  UPLOAD/DELETE FILE:
      --fdelete=FILE_ID           delete file FILE_ID
      --fupload=FILENAME          upload FILENAME
                                  can be used in conjuction with '--fpurpose=PURPOSE' option
  INFO:
      --finfo=FILE_ID             retrieve information about FILE_ID
      --flist                     list uploaded files
                                  used in conjuction with '--fpurpose=PURPOSE' option return files with the given purpose
      --fpurpose=PURPOSE          intended use of the uploaded file
                                  PURPOSE must be one of 'assistants', 'vision', 'batch', 'batch_output' and 'fine-tune'
				  can be used in conjuction with '--flist' or '--fupload=FILENAME' options
				  Note: a file can be uploaded for any purpose, but gptcli it is able to use only 'batch' purpose
  RETRIEVE/SAVE FILE:
      --fretrieve=FILE_ID         retrieve FILE_ID content
                                  use '--fsave=FILENAME' or shell redirection to save the content
      --fsave=FILENAME            save FILE_ID content retrieved with '--fretrieve=FILE_ID' option to FILENAME
                                  must be used in conjuction with '--fretrieve=FILE_ID' option
`

const helpMsgTips = "  💡                              %s --count in,out,total --model gpt-4 \"Summarize the following text: {TEXT}\"\n                                  %[1]s --batch-prepare --batch--id \"image-caption\" --vision \"image.jpeg\" \"Create a caption for the image\"\n"

// befor this var, always print cmd name, without newline char (os.Args[0]). eg: fmt.Fprintf(os.Stdout, "%s", os.Args[0]); fmt.Println(verMsg)
const verMsg = ` (gimlab) 1.7.14
Copyright (C) 2024 gimlab.
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.`

const jsonPayloadExamples = `[FUNCTION JSON OBJECTS EXAMPLES]

// --- one line --- //
{"name":"get_current_weather","description":"Get the current weather","parameters":{"type":"object","properties":{"location":{"type":"string","description":"The city and state, e.g. San Francisco, CA"},"format":{"type":"string","enum":["celsius","fahrenheit"],"description":"The temperature unit to use. Infer this from the users location."}},"required":["location","format"]}}

// --- pretty print --- //
{
  "name": "get_current_weather",
  "description": "Get the current weather",
  "parameters": {
    "type": "object",
    "properties": {
      "location": {
        "type": "string",
        "description": "The city and state, e.g. San Francisco, CA"
      },
      "format": {
        "type": "string",
        "enum": [
          "celsius",
          "fahrenheit"
        ],
        "description": "The temperature unit to use. Infer this from the users location."
      }
    },
    "required": [
      "location",
      "format"
    ]
  }
}

// --- tips --- //
[LINUX]
If you are using a Linux terminal with Bash, follow these steps:

1. create JSON file, eg. function.json, with your data, formatted as the examples above.
2. create a var to hold json data. Eg. if var name is "FUNC" and filename is "function.json": 
   export FUNC=$(cat function.json)
3. use your var with command:
   gptcli --function "$FUNC" "PROMPT"

If you prefer, create json file, as in step 1, and skip steps 2, 3 and 4. Then:
   gptcli --function "$(cat function.json)" "PROMPT"

Whichever method you choose, always remember the double quotes:
"$FUNC", "$(cat function.json)", "PROMPT", etc...

[WINDOWS]
If you are using Windows with CMD shell, follow these steps:

1. create JSON file, eg. function.json, with your data, formatted as the examples above.
2. all double quotes in your json data must be escaped with backslash (\). Eg:

{\"name\":\"get_current_weather\",\"description\":\"Get the current weather\",\"parameters\":{\"type\":\"object\",\"properties\":{\"location\":{\"type\":\"string\",\"description\":\"The city and state, e.g. San Francisco, CA\"},\"format\":{\"type\":\"string\",\"enum\":[\"celsius\",\"fahrenheit\"],\"description\":\"The temperature unit to use. Infer this from the users location.\"}},\"required\":[\"location\",\"format\"]}}

3. create a var to hold json data. Eg. if var name is "FUNC" and filename is "function.json": 
   FOR /F "tokens=*" %g IN ('type function.json') do (SET FUNC=%g)
4. use your var with command:
   gptcli.exe --function "%FUNC%" "PROMPT"

Whichever method you choose, always remember the single or double quotes:
"tokens=*", 'type function.json', "%FUNC%" "PROMPT", etc...

// --- notes --- //
- The same tips can be applied for logit bias option '--lb' and for all PROMPTs (user, system, etc...).
- For more advanced functions usage, such as parallel function calling, see the online documentation:
  https://gitlab.com/ai-gimlab/gptcli/-/tree/main/examples?ref_type=heads#functions
  and
  https://gitlab.com/ai-gimlab/gptcli/-/tree/main/examples?ref_type=heads#parallel-function-calling
`

const defaultValues string = `Defaults:
  model: gpt-4o-mini
  model embeddings: text-embedding-3-small
  model vision: gpt-4o
  model moderation: text-moderation-latest
  function call if no functions are required: none
  function call if functions are required: auto
  parallel tool calls: true
  temperature: 0.0
  top_p: 1.0
  presence_penalty: 0.0
  frequency_penalty: 0.0
  seed: null
  vision detail: auto
  max response tokens: 1000
  max text response tokens for vision: 300
  number of responses: 1
  response format: text
  stream response mode: false
  connection timeout: 120 sec
  connection timeout stream mode (chunk, total): 5 sec, 180 sec
  number of retries: 0
  wait before retry: 10 sec
  logprobs: false
  top logprobs: 0
  embeddings csv filename: data.csv
  embeddings encoding format: float
  batch input filename: batch_input.jsonl
  batch url: selected based on the task, either completions or embeddings
  batch (prefix) id: request
  batch endpoint: /v1/chat/completions
  batch list limit: 20`
