# CODE CHECK RESULTS

---

## // --- GOSEC --- //

```text
[gosec] 2024/08/21 14:15:05 Including rules: default
[gosec] 2024/08/21 14:15:05 Excluding rules: default
[gosec] 2024/08/21 14:15:05 Import directory: /home/gianluca/Documents/Development/go/src/aiGptcli
[gosec] 2024/08/21 14:15:06 Checking package: main
[gosec] 2024/08/21 14:15:06 Checking file: /home/gianluca/Documents/Development/go/src/aiGptcli/funcCompute.go
[gosec] 2024/08/21 14:15:08 Checking file: /home/gianluca/Documents/Development/go/src/aiGptcli/funcConfig.go
[gosec] 2024/08/21 14:15:10 Checking file: /home/gianluca/Documents/Development/go/src/aiGptcli/funcJSON.go
[gosec] 2024/08/21 14:15:11 Checking file: /home/gianluca/Documents/Development/go/src/aiGptcli/funcSelector.go
[gosec] 2024/08/21 14:15:12 Checking file: /home/gianluca/Documents/Development/go/src/aiGptcli/funcWeb.go
[gosec] 2024/08/21 14:15:14 Checking file: /home/gianluca/Documents/Development/go/src/aiGptcli/hdrData.go
[gosec] 2024/08/21 14:15:14 Checking file: /home/gianluca/Documents/Development/go/src/aiGptcli/hdrMessages.go
[gosec] 2024/08/21 14:15:14 Checking file: /home/gianluca/Documents/Development/go/src/aiGptcli/hdrOpt.go
[gosec] 2024/08/21 14:15:18 Checking file: /home/gianluca/Documents/Development/go/src/aiGptcli/hdrPresets.go
[gosec] 2024/08/21 14:15:19 Checking file: /home/gianluca/Documents/Development/go/src/aiGptcli/main.go
Results:


Summary:
  Gosec  : dev
  Files  : 10
  Lines  : 5500
  Nosec  : 0
  Issues : 0
```

---

## // --- GO VET --- //

```text
go vet: OK
```

---

## // --- GOVULNCHECK --- //

```text
No vulnerabilities found.
```

---

## // --- MEMORY ALIGNMENT --- //

```text
fieldalignment: OK
```

---
