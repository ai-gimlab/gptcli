#!/bin/bash

# data source
export BATCH_INPUT="batch_input.jsonl"
export BATCH_OUTPUT="batch_output.jsonl"

# count batch output lines
export LINES=$(wc -l $BATCH_OUTPUT | cut -d " " -f 1)

# write headers
echo "mood,prompt"

# loop through batch output lines
for i in $(seq 1 $LINES); do
  # query filter
  CUSTOM_ID="request-$i"

  # extract response from batch output, using $CUSTOM_ID as filter, and remove newline char
  jq -r --arg custom_id "$CUSTOM_ID" 'select(.custom_id == $custom_id) | .response.body.choices[0].message.content' $BATCH_OUTPUT | tr -d "\n"

  # add field separator (comma)
  echo -n ","
  # PROMPT prefix (double quotes)
  echo -en "\""

  # extract PROMPT from batch input, using $CUSTOM_ID as filter
  # remove delimiters chars '###' and newline char: from {###TEXT_PROMPT###\n} to {TEXT_PROMPT}
  jq -r --arg custom_id "$CUSTOM_ID" 'select(.custom_id == $custom_id) | .body.messages[1].content' $BATCH_INPUT | tr -d "#" | tr -d "\n"

  # PROMPT suffix (double quotes)
  echo -e "\""
done

