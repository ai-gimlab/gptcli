import json
import csv
import pandas as pd

# files
batch_input = 'batch_input.jsonl'
batch_output = 'batch_output.jsonl'
batch_results = 'batch_results.csv'

# config pandas: columns width
pd.options.display.max_colwidth = 100

# Load the JSONL files
with open(batch_input, 'r') as f:
    requests = [json.loads(line) for line in f]

with open(batch_output, 'r') as f:
    responses = [json.loads(line) for line in f]

# Create a dictionary to map custom_id to the corresponding request and response
data = {}
for request in requests:
    data[request['custom_id']] = {'prompt': request['body']['messages'][1]['content']}

for response in responses:
    data[response['custom_id']]['mood'] = response['response']['body']['choices'][0]['message']['content']

# Write the output CSV file
with open(batch_results, 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerow(['mood', 'prompt'])
    for custom_id in data:
        s = data[custom_id]['prompt'].strip("#")
        writer.writerow([data[custom_id]['mood'], s])

# create and print dataframe
df = pd.read_csv(batch_results)
print(df)
