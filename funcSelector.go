package main

import (
	"bytes"
	"fmt"
	urlchk "net/url"
	"regexp"
	"strings"

	"github.com/ericchiang/css"
	"golang.org/x/net/html"
)

func selectHTML(urlAddr, selector string) (selected string, err error) {
	// function name to return if any error occur
	var funcName string = "selectHTML"

	var buf string
	var defaultSelectors []string = []string{"main", "body"}

	// check given URL address format
	if err := checkFmtURL(urlAddr); err != nil {
		return "", err
	}

	// make HTTP request
	responseBody, err := httpRequest(method, urlAddr, false)
	if err != nil {
		return "", err
	}

	// return full response, with all HTML tags
	if strings.ToUpper(selector) == "NONE" {
		return responseBody, nil
	}

	// parse HTML tree
	node, err := html.Parse(strings.NewReader(responseBody))
	if err != nil {
		return "", fmt.Errorf("func %q - error parsing HTML nodes tree: %w\n", funcName, err)
	}

	// check if 'selector' was given as cmdline arg
	switch {
	case selector != "":
		// cmdline arg
		selectedText, err := findSelector(selector, node)
		if err != nil {
			return "", err
		}
		buf = StripHTMLTags(selectedText)
	default:
		// default selectors: main, body
		for _, sel := range defaultSelectors {
			selectedText, err := findSelector(sel, node)
			if err == nil || strings.Contains(err.Error(), "selector not found") {
				buf = StripHTMLTags(selectedText)
				if len(buf) > 0 {
					break
				}
			} else {
				return "", err
			}
		}
	}

	if len(buf) == 0 {
		return "", fmt.Errorf("func %q - selector not found: %q\n", funcName, selector)
	}

	return sanitizeString(buf), nil
}

func StripHTMLTags(html string) (stripped string) {
	// non-breaking space characters
	re := regexp.MustCompile(`&nbsp;`)
	stripped = re.ReplaceAllString(html, " ")

	// remove any HTML tag, including empty tags and self-closing tags
	re = regexp.MustCompile(`<.*?>`)
	stripped = re.ReplaceAllString(stripped, "")

	return stripped
}

func RemoveLeadingWhitespace(text string) string {
	re := regexp.MustCompile(`(?m)^( +)`)
	stripped := re.ReplaceAllString(text, "")
	return stripped
}

func RemoveDoubleCurlyBraces(text string) string {
	re := regexp.MustCompile(`\{\{[^}]*\}\}`)
	stripped := re.ReplaceAllString(text, "")
	return stripped
}

// select data using given selector
func findSelector(selector string, node *html.Node) (selectedData string, err error) {
	// function name to return if any error occur
	var funcName string = "findSelector"

	var temp bytes.Buffer

	sel, err := css.Parse(selector)
	if err != nil {
		return "", fmt.Errorf("func %q - error during SELECTOR creation: %w\n", funcName, err)
	}

	for _, ele := range sel.Select(node) {
		if err := html.Render(&temp, ele); err != nil {
			return "", fmt.Errorf("func %q - error rendering HTML page: %w\n", funcName, err)
		}
	}

	if temp.Len() == 0 {
		return "", fmt.Errorf("func %q - selector not found: %q\n", funcName, selector)
	}

	return string(temp.String()), nil
}

// remove redundant newlines, ctrl/esc chars, ascii > 255, etc...
func sanitizeString(buf string) (sanitizedString string) {
	var asciiChars rune = 1 << 8 // use only first 256 ascii chars (extended ascii)

	// multiple newlines
	re := regexp.MustCompile(`\n+`)
	buf = re.ReplaceAllString(buf, "\n")

	// multiple white spaces
	re = regexp.MustCompile(`\s{2,}`)
	buf = re.ReplaceAllString(buf, " ")

	// remove white space followed by newline
	re = regexp.MustCompile(`\s{1,}\n`)
	buf = re.ReplaceAllString(buf, "")

	replaceArgs := [][]string{
		{"&#34;", "\""},
		{"&#39;", "'"},
		{"¶", ""},
	}

	for _, v := range replaceArgs {
		var temp string = ""
		temp = strings.ReplaceAll(buf, v[0], v[1])
		buf = temp
	}

	r := []rune(strings.TrimSpace(buf))
	s := make([]rune, 0)

	for _, v := range r {
		if v < asciiChars {
			s = append(s, v)
		}
	}

	sanitizedString = RemoveLeadingWhitespace(string(s))
	sanitizedString = RemoveDoubleCurlyBraces(sanitizedString)

	return sanitizedString
}

// validate URL format
func checkFmtURL(urlAddr string) error {
	// function name to return if any error occur
	var funcName string = "checkFmtURL"

	if _, err := urlchk.Parse(urlAddr); err != nil {
		return fmt.Errorf("func %q - %q wrong URL format: %w\n", funcName, urlAddr, err)
	}
	return nil
}
