package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"strings"
	"time"

	"github.com/pborman/getopt/v2"
)

func configFlags() (err error) {
	// function name to return if any error occur
	var funcName string = "configFlags"

	// only for testing purpose
	if flagTestTimeout {
		url = urlCompletionTimeoutTest0
	}

	// ------ model selection (if requested, otherwise use default model) ------ //
	if (getopt.GetCount(flagModelLong) > 0 || getopt.GetCount(flagModelShort) > 0) && flagModel != "" {
		// check if model exist
		if !flagModeration && !flagModerationBool {
			if err := checkModelName(flagModel); err != nil {
				return err
			}
		}
		// config model
		switch {
		case flagEmbed:
			// model embedding
			modelEmbed = flagModel
		case flagFunction != "":
			// model function
			modelFunction = flagModel
		case flagModeration || flagModerationBool:
			// model moderation
			modelModeration = flagModel
		case flagVision != "":
			// model vision
			modelVision = flagModel
		default:
			// model language
			model = flagModel
		}
	}

	// ------ csv ------ //
	if flagCsv != "" && !flagPreview {
		// if csv file does not exist ask permission to create a new one
		var r rune
		_, errFileNotExist := os.Open(flagCsv)
		if newFileCSV = errors.Is(errFileNotExist, fs.ErrNotExist); newFileCSV {
			// ask to confirm new file creation
			fmt.Printf("File %q does not exist. Create new one (%q to quit and exit)? [y/n/q]: ", flagCsv, "q")
			if _, err := fmt.Scanf("%c", &r); err != nil {
				return fmt.Errorf("func %q - error creating CSV file %q: %w", funcName, flagCsv, err)
			}
			switch {
			case r == 'y' || r == 'Y':
				newFileCSV = true
			case r == 'n' || r == 'N':
				flagCsv = ""
			case r == 'q' || r == 'Q':
				return (fmt.Errorf("EXIT"))
			default:
				return fmt.Errorf("wrong option: %c\nexit program", r)
			}
		}
	}

	// ------ batch jobs ------ //
	// batch prepare
	if flagBatchInput != "" {
		// if jsonl file does not exist ask permission to create a new one
		var r rune
		_, errFileNotExist := os.Open(flagBatchInput)
		if newFileJSONL = errors.Is(errFileNotExist, fs.ErrNotExist); newFileJSONL {
			// ask to confirm new file creation
			fmt.Printf("File %q does not exist. Create new one (%q to quit and exit)? [y/n/q]: ", flagBatchInput, "q")
			if _, err := fmt.Scanf("%c", &r); err != nil {
				return fmt.Errorf("func %q - error creating  file %q: %w", funcName, flagBatchInput, err)
			}
			switch {
			case r == 'y' || r == 'Y':
				newFileJSONL = true
			case r == 'n' || r == 'N':
				flagBatchInput = defaultBatchInputFile
				fmt.Printf("using default file %s\n", defaultBatchInputFile)
			case r == 'q' || r == 'Q':
				return (fmt.Errorf("EXIT"))
			default:
				return fmt.Errorf("wrong option: %c\nexit program", r)
			}
		}
		batchInputFile = flagBatchInput
	}
	if flagBatchId != "" {
		batchCustomIdPrefix = flagBatchId + "-"
	}
	if batchCustomIdSeq, err = countFileLines(batchInputFile); err != nil && !errors.Is(err, fs.ErrNotExist) {
		return fmt.Errorf("func %q - reading file %q: %w", funcName, batchInputFile, err)
	}
	batchCustomId = fmt.Sprintf("%s%d", batchCustomIdPrefix, batchCustomIdSeq+1)
	// create batch
	if flagBatchCreate != "" {
		batchFileID = flagBatchCreate
		if flagBatchEndpoint != "" {
			batchEndpoint = flagBatchEndpoint
		}
		url = urlBatch
	}
	// check batch status
	if flagBatchStatus != "" {
		url = urlBatch + "/" + flagBatchStatus
	}
	// list batches
	if flagBatchList {
		var parameters string
		switch {
		case getopt.GetCount(flagBatchListLimitLong) > 0 && getopt.GetCount(flagBatchListAfterLong) > 0:
			parameters = fmt.Sprintf("?limit=%d&after=%s", flagBatchListLimit, flagBatchListAfter)
		case getopt.GetCount(flagBatchListLimitLong) > 0:
			parameters = fmt.Sprintf("?limit=%d", flagBatchListLimit)
		case getopt.GetCount(flagBatchListAfterLong) > 0:
			parameters = fmt.Sprintf("?after=%s", flagBatchListAfter)
		default:
			parameters = fmt.Sprintf("?limit=%d", defaultBatchListLimit)
		}
		url = urlBatch + parameters
	}
	// cancel batch
	if flagBatchCancel != "" {
		url = urlBatch + "/" + flagBatchCancel + "/cancel"
	}
	// save batch result to file
	if flagBatchResult != "" {
		fileApiSave = flagBatchResult
	}

	// ------ FileAPI ------ //
	// upload
	if flagFileApiUpload != "" {
		f, err := os.Open(flagFileApiUpload)
		if err != nil {
			return fmt.Errorf("func %q - reading file %q: %w", funcName, flagFileApiUpload, err)
		}
		defer f.Close()
		url = urlFileApi
		if flagFileApiUploadPurpose != "" {
			if _, ok := fileApiUploadPurposes[flagFileApiUploadPurpose]; !ok {
				return fmt.Errorf("func %q - wrong File API upload purpose: %q", funcName, flagFileApiUploadPurpose)
			}
			fileApiUploadPurpose = flagFileApiUploadPurpose
		}
		return nil
	}
	// retrieve file content
	if flagFileApiRetrieve != "" {
		url = urlFileApi + "/" + flagFileApiRetrieve + "/content"
		// save file
		if flagFileApiSave != "" {
			fileApiSave = flagFileApiSave
		}
		return nil
	}
	// list files
	if flagFileApiList {
		url = urlFileApi
		if flagFileApiUploadPurpose != "" {
			if _, ok := fileApiUploadPurposes[flagFileApiUploadPurpose]; !ok {
				return fmt.Errorf("func %q - wrong File API upload purpose: %q", funcName, flagFileApiUploadPurpose)
			}
			url = url + "?purpose=" + flagFileApiUploadPurpose
		}
		return nil
	}
	// file information
	if flagFileApiInfo != "" {
		url = urlFileApi + "/" + flagFileApiInfo
		return nil
	}
	// delete file
	if flagFileApiDelete != "" {
		url = urlFileApi + "/" + flagFileApiDelete
		method = "DELETE"
		return nil
	}

	// ------ embeddings ------ //
	if flagEmbed {
		url = urlEmbed
		inputs = strings.Split(prompt, objectSplitChar)
		batchEndpoint = batchEndpointEmbeddings
		if flagEmbedEncodingFormat {
			embedEncodingFormat = "base64"
		}
		if modelEmbed == embedDimensionsUnsupportedModel && getopt.GetCount(flagEmbedDimensionsLong) > 0 {
			fmt.Printf("embedding dimensions not supported for model %q, ignoring option \"--%s\"\n\n", embedDimensionsUnsupportedModel, flagEmbedDimensionsLong)
		}
		if (getopt.GetCount(flagEmbedDimensionsLong) > 0 && flagEmbedDimensions < minEmbeddingDimensions) && modelEmbed != embedDimensionsUnsupportedModel {
			return fmt.Errorf("wrong value for \"--%s\" option: %d\nminimum embedding dimensions value: %d", flagEmbedDimensionsLong, flagEmbedDimensions, minEmbeddingDimensions)
		}
		return nil
	}

	// ------ configs ------ //
	if flagStream {
		stream = true
		timeout = timeoutCompletionStream
	}
	if flagTimeout != 0 {
		timeout = time.Second * time.Duration(flagTimeout)
	}
	if flagTimeoutChunk != 0 {
		timeoutChunk = time.Second * time.Duration(flagTimeoutChunk)
	}
	if getopt.GetCount(flagRetriesLong) > 0 {
		retries = flagRetries
	}
	if getopt.GetCount(flagRetriesWaitLong) > 0 {
		retriesWait = flagRetriesWait
	}
	if flagModeration || flagModerationBool {
		url = urlModeration
	}
	if flagFunction != "" {
		functionCall = "auto"
		functionList = strings.Split(flagFunction, objectSplitChar)
	}
	if flagFunctionCall != "" {
		functionCall = flagFunctionCall
	}
	if flagTool != "" {
		// check if string is a valid json object
		if err := isValidJSON(flagTool); err != nil {
			return fmt.Errorf("func %q - invalid 'function' JSON format: %w\nJSON data: %s", funcName, err, flagTool)
		}
		toolContentList = strings.Split(flagToolContent, objectSplitChar)
	}
	if flagVision != "" {
		switch {
		case strings.Contains(flagVision, ","):
			visionImagesArgs = strings.Split(flagVision, ",")
		default:
			visionImagesArgs = append(visionImagesArgs, flagVision)
		}
		if err := validateVision(visionImagesArgs); err != nil {
			return err
		}
		maxTokens = maxTokensVision
	}
	if flagVisionDetail != "" {
		if strings.ToLower(flagVisionDetail) != "low" && strings.ToLower(flagVisionDetail) != "high" {
			return fmt.Errorf("func %q - invalid vision detail: %s\ndetail can be %q or %q", funcName, flagVisionDetail, "low", "high")
		}
		visionDetail = flagVisionDetail
	}
	if flagTemperature != 0 && (flagTemperature >= minLimitDefault && flagTemperature <= maxTemperatureLimit) {
		temperature = flagTemperature
	}
	if flagTemperature != 0 && (flagTemperature < minLimitDefault || flagTemperature > maxTemperatureLimit) {
		return fmt.Errorf("func %q - wrong parameter for -%c or --%s: %.2f (less than %.2f or greater than %.2f)", funcName, flagTemperatureShort, flagTemperatureLong, flagTemperature, minLimitDefault, maxTemperatureLimit)
	}
	if flagTopP != 0 && (flagTopP >= minLimitDefault && flagTopP <= maxTopPLimit) {
		top_p = flagTopP
	}
	if flagTopP != 0 && (flagTopP < minLimitDefault || flagTopP > maxTopPLimit) {
		return fmt.Errorf("func %q - wrong parameter for --%s: %.2f (less than %.2f or greater than %.2f)", funcName, flagTopPLong, flagTopP, minLimitDefault, maxTopPLimit)
	}
	if flagPresencePenalty != 0 && (flagPresencePenalty >= minPenaltyLimit && flagPresencePenalty <= maxPenaltyLimit) {
		presencePenalty = flagPresencePenalty
	}
	if flagPresencePenalty != 0 && (flagPresencePenalty < minPenaltyLimit || flagPresencePenalty > maxPenaltyLimit) {
		return fmt.Errorf("func %q - wrong parameter for --%s: %.2f (less than %.2f or greater than %.2f)", funcName, flagPresencePenaltyLong, flagPresencePenalty, minPenaltyLimit, maxPenaltyLimit)
	}
	if flagFrequencyPenalty != 0 && (flagFrequencyPenalty >= minPenaltyLimit && flagFrequencyPenalty <= maxPenaltyLimit) {
		frequencyPenalty = flagFrequencyPenalty
	}
	if flagFrequencyPenalty != 0 && (flagFrequencyPenalty < minPenaltyLimit || flagFrequencyPenalty > maxPenaltyLimit) {
		return fmt.Errorf("func %q - wrong parameter for --%s: %.2f (less than %.2f or greater than %.2f)", funcName, flagFrequencyPenaltyLong, flagFrequencyPenalty, minPenaltyLimit, maxPenaltyLimit)
	}
	if getopt.GetCount(flagSeedLong) > 0 {
		seed = flagSeed
	}
	if flagResponses != 0 {
		numberOfResponses = flagResponses
	}
	if flagCount != "" {
		if !regexCompileCountOption.MatchString(flagCount) {
			return fmt.Errorf("wrong argument for option -%c or option --%s: %s", flagCountShort, flagCountLong, flagCount)
		}
	}
	if flagName != "" {
		authorName = flagName
	}
	if flagUser != "" {
		userID = flagUser
	}
	if flagResponseFormat != "" {
		s := strings.ToLower(flagResponseFormat)
		switch {
		case s != "text" && s != "json" && s != "json_object":
			return fmt.Errorf("wrong argument for option --%s: %s", flagResponseFormatLong, flagResponseFormat)
		default:
			if s == "json" {
				s = "json_object"
			}
			responseFormat = s
		}
	}
	if flagPreset != "" {
		var p presetSettings
		if err := p.set(flagPreset); err != nil {
			return err
		}
	}
	if flagWeb != "" {
		if selected, err := selectHTML(flagWeb, flagWebSelect); err != nil {
			return err
		} else {
			webPageContent = selected
		}
	}
	if flagResTok != 0 {
		maxTokens = flagResTok
	}
	if flagLogProbs {
		logProbs = true
		if getopt.GetCount(flagTopLogProbsLong) > 0 {
			if flagTopLogProbs < 0 || flagTopLogProbs > maxTopLogProbsLimit {
				return fmt.Errorf("func %q - wrong parameter for --%s: %d (less than %d or greater than %d)", funcName, flagTopLogProbsLong, flagTopLogProbs, int(minLimitDefault), maxTopLogProbsLimit)
			}
			topLogProbs = flagTopLogProbs
		}
	}
	return nil
}

func configRoles() error {
	// function name to return if any error occur
	var funcName string = "configRoles"

	// ROLE: system
	if flagSystem != "" || flagPreset != "" || flagWeb != "" || flagFunction != "" {
		switch {
		case flagVision != "":
			// json structure for vision is different from non-vision structure
			var contents []content
			var requestMessage requestMessageVision
			var contentMessageText content
			contentMessageText.Type = "text"
			switch {
			case flagPreset != "" && flagSystem == "":
				contentMessageText.Text = presets[flagPreset].systemMessage
			default:
				s := strings.ReplaceAll(flagSystem, "\\n", "\n")
				contentMessageText.Text = s
			}
			contents = append(contents, contentMessageText)
			requestMessage.Role = system
			requestMessage.Content = contents
			requestMessagesVision = append(requestMessagesVision, requestMessage)
		default:
			var systemMessage requestMessage
			systemMessage.Role = system
			systemMessage.Name = authorName
			switch {
			case flagPreset != "" && flagSystem == "":
				systemMessage.Content = presets[flagPreset].systemMessage
			case flagWeb != "" && flagSystem == "":
				systemMessage.Content = systemMessageWeb
			default:
				s := strings.ReplaceAll(flagSystem, "\\n", "\n")
				systemMessage.Content = s
			}
			requestMessages = append(requestMessages, systemMessage)
		}
	}
	// common data for one round chat/function_call
	if flagAssistant != "" || flagTool != "" {
		switch {
		case flagVision != "" && flagTool == "":
			// json structure for vision is different from non-vision structure
			var contents []content
			var requestMessage requestMessageVision
			var contentMessageText content
			contentMessageText.Type = "text"
			contentMessageText.Text = flagPreviousPrompt
			contents = append(contents, contentMessageText)
			requestMessage.Role = user
			requestMessage.Content = contents
			requestMessagesVision = append(requestMessagesVision, requestMessage)
		default:
			var userMessage requestMessage
			userMessage.Role = user
			userMessage.Content = flagPreviousPrompt
			userMessage.Name = authorName
			requestMessages = append(requestMessages, userMessage)
		}
	}
	// ROLE: assistant
	if flagAssistant != "" && flagTool == "" {
		switch {
		case flagVision != "":
			// json structure for vision is different from non-vision structure
			var contents []content
			var requestMessage requestMessageVision
			var contentMessageText content
			contentMessageText.Type = "text"
			contentMessageText.Text = flagAssistant
			contents = append(contents, contentMessageText)
			requestMessage.Role = assistant
			requestMessage.Content = contents
			requestMessagesVision = append(requestMessagesVision, requestMessage)
		default:
			var assistantMessage requestMessage
			assistantMessage.Role = assistant
			assistantMessage.Content = flagAssistant
			assistantMessage.Name = authorName
			requestMessages = append(requestMessages, assistantMessage)
		}
	}
	// ROLE: tool
	if (flagTool != "" && flagAssistant == "") && flagVision == "" {
		var calls []toolCall
		var assistantMessage requestMessage
		var toolMessages []requestMessage
		assistantMessage.Role = assistant
		// unmarshal single function object
		if err := json.Unmarshal([]byte(flagTool), &calls); err != nil {
			return fmt.Errorf("func %q - invalid 'tool' data: %s", funcName, flagFunction)
		}
		// config 'assistant' and 'tool' roles
		for i := range calls {
			if len(calls) != len(toolContentList) {
				return fmt.Errorf("func %q: option '--%s' has %d JSON objects, but option '--%s' has %d arguments", funcName, flagToolLong, len(calls), flagToolContentLong, len(toolContentList))
			}
			var toolMessage requestMessage
			assistantMessage.ToolCalls = append(assistantMessage.ToolCalls, calls[i])
			toolMessage.Role = tool
			toolMessage.Name = assistantMessage.ToolCalls[i].Function.Name
			toolMessage.ToolCallId = assistantMessage.ToolCalls[i].Id
			toolMessage.Content = toolContentList[i]
			toolMessages = append(toolMessages, toolMessage)
		}
		// add 'assistant' and 'tool' messages to request
		requestMessages = append(requestMessages, assistantMessage)
		requestMessages = append(requestMessages, toolMessages...)

		return nil
	}
	// ROLE: user
	if flagVision != "" || strings.Contains(model, modelVisionSubStr) {
		// vision task, or vision model for language task, requested
		var contents []content
		var requestMessage requestMessageVision
		var contentMessageText content
		var contentMessageImageURL content
		contentMessageText.Type = "text"
		contentMessageText.Text = prompt
		contents = append(contents, contentMessageText)
		if len(visionImagesURLs) > 0 {
			// remote image files
			for i := range visionImagesURLs {
				var imageURL imageUrl
				contentMessageImageURL.Type = "image_url"
				imageURL.Url = visionImagesURLs[i]
				imageURL.Detail = visionDetail
				contentMessageImageURL.ImageUrl = &imageURL
				contents = append(contents, contentMessageImageURL)
			}
		}
		if len(visionImagesLocal) > 0 {
			// local image files
			for i := range visionImagesLocal {
				var imageLocal imageUrl
				contentMessageImageURL.Type = "image_url"
				imageLocal.Url = imageBase64Prefix + visionImagesLocal[i]
				imageLocal.Detail = visionDetail
				contentMessageImageURL.ImageUrl = &imageLocal
				contents = append(contents, contentMessageImageURL)
			}
		}
		requestMessage.Role = user
		requestMessage.Content = contents
		requestMessagesVision = append(requestMessagesVision, requestMessage)
		return nil
	}

	// normal language task
	var userMessage requestMessage
	userMessage.Role = user
	userMessage.Name = authorName
	userMessage.Content = prompt
	requestMessages = append(requestMessages, userMessage)

	return nil
}

func userPromptExceptions() error {
	// function name to return if any error occur
	var funcName string = "userPromptExceptions"

	var b strings.Builder
	var writeErr []error = make([]error, 0)
	switch {
	case flagWebTest:
		prompt = webPageContent
		return nil
	case flagPreset != "" && flagWeb != "":
		_, err := b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(webPageContent)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		prompt = b.String()
	case flagPreset != "":
		_, err := b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(prompt)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		prompt = b.String()
	case flagWeb != "":
		_, err := b.WriteString(delimiterWeb)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(webPageContent)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiterWeb)
		writeErr = append(writeErr, err)
		_, err = b.WriteString("\n\n")
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(prompt)
		writeErr = append(writeErr, err)
		_, err = b.WriteString(delimiter)
		writeErr = append(writeErr, err)
		prompt = b.String()
	}
	if len(writeErr) > 0 {
		for _, e := range writeErr {
			if e != nil {
				return fmt.Errorf("func %q - error composing web prompt: %w", funcName, e)
			}
		}
	}
	return nil
}
