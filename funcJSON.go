package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"sort"
	"strings"
)

// create payloads
func createPayloadJSON(input string) (payloadJSON []byte, err error) {
	// function name to return if any error occur
	var funcName string = "createPayloadJSON"

	// configure model request
	switch {
	case flagEmbed:
		// embeddings
		payload := &requestEmbed{
			Input:          inputs,
			Model:          modelEmbed,
			EncodingFormat: embedEncodingFormat,
			User:           flagUser,
		}

		// embedding vector size
		if payload.Model != embedDimensionsUnsupportedModel && flagEmbedDimensions > 0 {
			payload.Dimensions = flagEmbedDimensions
		}

		// prepare batch payload, if requested
		var prepare requestPrepareBatch
		if flagBatchPrepare {
			prepare.Body = payload
			prepare.CustomId = batchCustomId
			prepare.Method = "POST"
			prepare.Url = batchEndpoint
		}

		// create JSON payload
		var errMarshal error
		switch {
		case flagBatchPrepare:
			payloadJSON, errMarshal = json.Marshal(prepare)
		default:
			payloadJSON, errMarshal = json.Marshal(payload)
		}
		if errMarshal != nil {
			return nil, fmt.Errorf("func %q - error creating embeddings JSON payload: %w\n", funcName, errMarshal)
		}
		//return payloadJSON, nil
	case flagModeration || flagModerationBool:
		// moderation
		if len(input) == 0 {
			return nil, fmt.Errorf("func %q - missing user PROMPT\n", funcName)
		}

		payload := &moderationPrompt{
			Input: input,
			Model: modelModeration,
		}

		// create JSON payload
		payloadJSON, err = json.Marshal(payload)
		if err != nil {
			return nil, fmt.Errorf("func %q - error creating moderation JSON payload: %w\n", funcName, err)
		}
	case flagBatchCreate != "":
		payload := &createBatch{
			InputFileId:      batchFileID,
			Endpoint:         batchEndpoint,
			CompletionWindow: batchCompletionWindow,
		}

		// metadata
		if flagBatchMetadata != "" {
			// unmarshal metadata object
			if err := json.Unmarshal([]byte(flagBatchMetadata), &payload.Metadata); err != nil {
				return nil, fmt.Errorf("func %q - invalid metadata: %s\n", funcName, flagBatchMetadata)
			}
		}

		// create JSON payload
		payloadJSON, err = json.Marshal(payload)
		if err != nil {
			return nil, fmt.Errorf("func %q - error creating batch JSON payload: %w\n", funcName, err)
		}
	default:
		// completion task, tool, function
		payload := &request{
			Temperature:       temperature,
			TopP:              top_p,
			PresencePenalty:   presencePenalty,
			FrequencyPenalty:  frequencyPenalty,
			Seed:              seed,
			NumberOfResponses: numberOfResponses,
			Stream:            stream,
			Logprobs:          logProbs,
			TopLogprobs:       topLogProbs,
			MaxTokens:         maxTokens,
			User:              userID,
			ResponseFormat: &respFormat{
				Type: responseFormat,
			},
		}

		switch {
		case flagVision != "":
			payload.Model = modelVision
			payload.Messages = requestMessagesVision
		default:
			payload.Model = model
			payload.Messages = requestMessages
		}

		// token count in stream mode
		if flagStream && flagCount != "" {
			var streamUsage streamOption
			streamUsage.IncludeUsage = true
			payload.StreamOption = &streamUsage
		}

		// compose stop sequence, if any
		if flagStop != "" {
			s := flagStop

			// sanitize string
			s = strings.TrimSpace(s)
			s = strings.TrimSuffix(s, ",")

			// check string for valid format
			if !regexCompileCommaSepList.MatchString(s) {
				return nil, fmt.Errorf("func %q - invalid 'stop' sequence: %s\n", funcName, flagStop)
			}

			// add stop sequences to payload
			var stopSequences []string = strings.SplitN(s, ",", -1)
			payload.Stop = stopSequences
		}

		// compose logit_bias, if any
		if flagLogitBias != "" {
			s := flagLogitBias

			// sanitize string
			s = strings.TrimSpace(s)

			// check if string is a valid json object
			if err := isValidJSON(s); err != nil {
				return nil, fmt.Errorf("func %q - invalid 'logit_bias' JSON format: %w\nJSON data: %s\n", funcName, err, s)
			}

			// decode logit bias
			var logitBias map[string]int
			if err := json.Unmarshal([]byte(s), &logitBias); err != nil {
				return nil, fmt.Errorf("func %q - invalid 'logit_bias' data: %s\n", funcName, flagLogitBias)
			}

			// check bias values
			for _, v := range logitBias {
				if v < minLogitBiasLimit || v > maxLogitBiasLimit {
					return nil, fmt.Errorf("func %q - 'logit_bias' values out of range (-100, 100): %s\n", funcName, flagLogitBias)
				}
			}

			// add logit bias to payload
			payload.LogitBias = logitBias
		}

		// functions
		if flagFunction != "" {
			if flagVision == "" {
				payload.Model = modelFunction
			}
			// add tool_choice MODE to payload
			// 'tool_choice' json key can be a string, such as 'none', 'auto', 'required', or an object, such as {"type: "function", "function": {"name": "my_function"}}
			switch {
			case functionCall != "auto" && functionCall != "none" && functionCall != "required":
				// json object: {"type: "function", "function": {"name": "my_function"}}
				f := &functionCallKey{
					Name: functionCall,
				}
				t := &toolChoiceMode{
					Type:     "function",
					Function: f,
				}
				f.Name = flagFunctionCall
				payload.ToolChoice = t
			default:
				// string: 'none', 'auto' or 'required'
				payload.ToolChoice = functionCall
			}

			// parallel function call TRUE/FALSE
			if flagParallelToolCalls {
				parallelToolCall = false
			}
			payload.ParallelToolCalls = parallelToolCall

			// parse functions list
			var tools []toolConfig
			for _, f := range functionList {
				// check if string is a valid json object
				if err := isValidJSON(f); err != nil {
					return nil, fmt.Errorf("func %q - invalid 'function' JSON format: %w\nJSON data: %s\n", funcName, err, f)
				}
				// create object to store function
				var t toolConfig
				t.Type = toolType
				// unmarshal single function object
				if err := json.Unmarshal([]byte(f), &t.Function); err != nil {
					return nil, fmt.Errorf("func %q - invalid 'function' data: %s\n", funcName, flagFunction)
				}
				// append function object to 'tools' (json) array
				tools = append(tools, t)
			}

			// add functions to payload
			payload.Tools = tools
		}

		// prepare batch payload, if requested
		var prepare requestPrepareBatch
		if flagBatchPrepare {
			prepare.Body = payload
			prepare.CustomId = batchCustomId
			prepare.Method = "POST"
			prepare.Url = batchEndpoint
		}

		// create JSON payload
		var errMarshal error
		switch {
		case flagBatchPrepare:
			payloadJSON, errMarshal = json.Marshal(prepare)
		default:
			payloadJSON, errMarshal = json.Marshal(payload)
		}
		if errMarshal != nil {
			return nil, fmt.Errorf("func %q - error creating completion JSON payload: %w\n", funcName, errMarshal)
		}

		// encode request data to csv
		if flagCsv != "" && payload != nil {
			requestCSV(payload)
		}
	}

	return payloadJSON, nil
}

// pretty print one line JSON
func prettyPrintJsonString(jsonString string) (formattedString string, err error) {
	// function name to return if any error occur
	var funcName string = "prettyPrintJsonString"

	var buf bytes.Buffer
	if err := json.Indent(&buf, []byte(jsonString), "", "  "); err != nil {
		return "", fmt.Errorf("func %q - error formatting JSON response: %w\n", funcName, err)
	}

	return buf.String(), nil
}

// parse 'moderation responses'
func extractModerationResponse(response string) (flagged bool, model string, err error) {
	// function name to return if any error occur
	var funcName string = "extractModerationResponse"

	var moderationResponse moderationResponse

	if err := json.Unmarshal([]byte(response), &moderationResponse); err != nil {
		return false, "", fmt.Errorf("func %q - error extracting JSON response: %w\n", funcName, err)
	}

	if len(moderationResponse.Results) > 0 {
		return moderationResponse.Results[0].Flagged, moderationResponse.Model, nil
	}

	return false, "", fmt.Errorf("func %q - moderation response is empty - wait a bit, then retry\n", funcName)
}

// check for valid JSON format
func isValidJSON(data string) error {
	var temp interface{}
	err := json.Unmarshal([]byte(data), &temp)
	return err
}

/*
Legacy from old API with 'function' as a separate object.
New API use 'tools' and 'function' is now one of many tools
This code was not deleted as it may be useful in the future

// convert multiline response json fields to csv fields (function 'arguments' field can be an example)
func convertMultiLineJSONtoCSV(jsonString string) (csvString string, err error) {
	// function name to return if any error occur
	var funcName string = "convertMultiLineJSONtoCSV"

	// prepare
	var b strings.Builder
	args := strings.Split(jsonString, "\n")

	// compose body
	if len(args) > 0 {
		for i, arg := range args[1 : len(args)-1] { // take a slice of args without leading and trailing curly braces
			arg = strings.TrimSpace(arg)
			if _, err := b.WriteString(arg); err != nil {
				return "", fmt.Errorf("func %q - error converting function arguments: %w\n", funcName, err)
			}
			if i < len(args[1:len(args)-1])-1 {
				if _, err := b.WriteString(" "); err != nil {
					return "", fmt.Errorf("func %q - error converting function arguments: %w\n", funcName, err)
				}
			}
		}
	}

	return b.String(), nil
}
*/

func listModels() error {
	// function name to return if any error occur
	var funcName string = "listModels"

	const gpt string = "gpt-"
	const instruct string = "instruct"
	const embeddings string = "embed"
	var modelsList modelsListData

	// request models list
	models, err := httpRequest(method, urlModelsList, true)
	if err != nil {
		return err
	}

	// validate response
	if err := isValidJSON(models); err != nil {
		return err
	}

	// decode response
	if err := json.Unmarshal([]byte(models), &modelsList); err != nil {
		return fmt.Errorf("func %q - error extracting JSON response: %w\n", funcName, err)
	}

	// extract and print data
	if len(modelsList.Data) > 0 {
		// sort and filter models name
		for _, m := range modelsList.Data {
			if (strings.Contains(m.Id, gpt) || strings.Contains(m.Id, embeddings)) && !strings.Contains(m.Id, instruct) {
				modelNames = append(modelNames, m.Id)
			}
		}
		sort.SliceStable(modelNames, func(i, j int) bool { return modelNames[i] < modelNames[j] })

		// this function can also be used by 'checkModelName()' function
		if !flagListModels {
			return nil
		}

		fmt.Println("chat/completion models:")
		for _, m := range modelNames {
			if strings.Contains(m, gpt) {
				fmt.Printf("  %s\n", m)
			}
		}

		fmt.Println()

		fmt.Println("embedding models:")
		for _, m := range modelNames {
			if strings.Contains(m, embeddings) {
				fmt.Printf("  %s\n", m)
			}
		}
		fmt.Printf("\n%s\n", modelsTechRef)
	} else {
		fmt.Printf("models data not available - retry later\n")
	}

	return nil
}
